package com.apps.ksullivan.weatherapp.listener;

import com.apps.ksullivan.weatherapp.model.City;

/**
 * Created by ksullivan on 12/20/17.
 */

public interface FetchCityByIdListener {
    void onCityReturned(City city);
}
