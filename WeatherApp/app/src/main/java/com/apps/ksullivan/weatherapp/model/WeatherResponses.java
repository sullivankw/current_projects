package com.apps.ksullivan.weatherapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Created by ksullivan on 12/20/17.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponses {
    @JsonProperty("list")
    private java.util.List<WeatherResponse> list;
    @JsonProperty("cnt")
    private int cnt;

    public int getCnt() {
        return cnt;
    }

    public void setCnt(int cnt) {
        this.cnt = cnt;
    }

    public List<WeatherResponse> getList() {
        return list;
    }

    public void setList(List<WeatherResponse> list) {
        this.list = list;
    }

}
