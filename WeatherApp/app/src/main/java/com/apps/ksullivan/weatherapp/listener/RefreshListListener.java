package com.apps.ksullivan.weatherapp.listener;

/**
 * Created by ksullivan on 12/21/17.
 */

public interface RefreshListListener {

    void onCityRefreshed();
}
