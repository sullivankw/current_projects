package com.apps.ksullivan.weatherapp.listener;

import com.apps.ksullivan.weatherapp.model.City;

import java.util.List;

/**
 * Created by ksullivan on 12/20/17.
 */

public interface FetchCityListListener {

    void onCityListFetched(List<City> cities);
}
