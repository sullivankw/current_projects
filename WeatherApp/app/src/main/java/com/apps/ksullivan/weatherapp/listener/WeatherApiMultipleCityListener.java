package com.apps.ksullivan.weatherapp.listener;

import com.apps.ksullivan.weatherapp.model.WeatherResponse;

import java.util.List;

/**
 * Created by ksullivan on 12/20/17.
 */

public interface WeatherApiMultipleCityListener {

    void onSuccessResponseReturnedFromAPIMultiCityCall(List<WeatherResponse> weatherResponseList);
}
