package com.apps.ksullivan.weatherapp.service;

import android.util.Log;

import com.apps.ksullivan.weatherapp.listener.WeatherApiGetByIdListener;
import com.apps.ksullivan.weatherapp.listener.WeatherApiListener;
import com.apps.ksullivan.weatherapp.listener.WeatherApiMultipleCityListener;
import com.apps.ksullivan.weatherapp.model.City;
import com.apps.ksullivan.weatherapp.model.WeatherResponse;
import com.apps.ksullivan.weatherapp.model.WeatherResponses;
import com.apps.ksullivan.weatherapp.network.WeatherApiClient;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by ksullivan on 12/19/17.
 */

public class WeatherServiceImpl {
    public static final String WEATHER_API_KEY = "3158c872759587a0e6e876b634e839f4";
    public static final String WEATHER_API_BASE_URL = "http://api.openweathermap.org/data/2.5/";
    private WeatherApiListener listener;
    private WeatherApiGetByIdListener weatherApiGetByIdListener;
    private WeatherApiMultipleCityListener multipleCityListener;

    public  void setWeatherApiMultipleCityListener(WeatherApiMultipleCityListener multipleCityListener) {
        this.multipleCityListener = multipleCityListener;
    }

    public void setWeatherApiListener( WeatherApiListener listener) {
        this.listener = listener;
    }

    public void setWeatherApiGetByIdListener(WeatherApiGetByIdListener weatherApiGetByIdListener) {
        this.weatherApiGetByIdListener = weatherApiGetByIdListener;

    }

    public WeatherApiClient getApiClient() {

        Retrofit.Builder builder = new Retrofit.Builder().baseUrl(WEATHER_API_BASE_URL)
                .addConverterFactory(GsonConverterFactory.create());
        Retrofit retrofit = builder.build();
        //Retrofit retrofit = builder.
        return retrofit.create(WeatherApiClient.class);
    }

    public void getWeatherById(String cityId) {
        WeatherApiClient client = getApiClient();
        Call<WeatherResponse> call = client.getWeatherById(cityId, WEATHER_API_KEY);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResponse w = response.body();
                    weatherApiGetByIdListener.onSuccessResponseReturnedFromAPIGetById(w);
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {

            }
        });

    }

    public void getWeatherByCity (String city) {

        WeatherApiClient client = getApiClient();
        Call<WeatherResponse> call = client.getWeatherByCityName(city, WEATHER_API_KEY);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResponse w = response.body();
                    listener.onSuccessResponseReturnedFromAPI(response.body());

                }
                if (response.body() == null) {
                    listener.onSuccessResponseReturnedFromAPI(null);
                }
            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {
                t.printStackTrace();

            }
        });

    }

    public void getWeatherByZip(String zip) {
        String zipAndCountryCode = zip + ",us";
        WeatherApiClient client = getApiClient();
        Call<WeatherResponse> call = client.getWeatherByZip(zipAndCountryCode, WEATHER_API_KEY);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {

                if (response.isSuccessful()) {
                    listener.onSuccessResponseReturnedFromAPI(response.body());

                }

                if (response.body() == null) {
                    listener.onSuccessResponseReturnedFromAPI(null);
                }

            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {

            }
        });

    }

    public void getWeatherByLocation(String lat, String lon) {
        //String zipAndCountryCode = zip + ",us";
        //lat=35&lon=139
        WeatherApiClient client = getApiClient();
        Call<WeatherResponse> call = client.getWeatherByCoordinates(lat, lon, WEATHER_API_KEY);
        call.enqueue(new Callback<WeatherResponse>() {
            @Override
            public void onResponse(Call<WeatherResponse> call, Response<WeatherResponse> response) {
                if (response.isSuccessful()) {
                    WeatherResponse w = response.body();
                    Log.i("locationResponse", response.body().toString());

                    listener.onSuccessResponseReturnedFromAPI(response.body());

                }

                if (response.body() == null) {
                    listener.onSuccessResponseReturnedFromAPI(null);
                    //do some error handinling
                }

            }

            @Override
            public void onFailure(Call<WeatherResponse> call, Throwable t) {


            }
        });

        //here could do something else like show a progress bar while the call is in progree
    }

    public void getWeatherForMultipleCities(List<City> cities) {
        StringBuilder sb = new StringBuilder();

        for (int k = 0; k <cities.size(); k++) {
            sb.append(cities.get(k).getCityId());
            if (k + 1 < cities.size()) {
                Log.i("myList", cities.get(k).getName() + cities.get(k).getCityId());
                sb.append(",");
            }
        }
        String query = sb.toString();

        WeatherApiClient client = getApiClient();
        String units = "metric";
        Call<WeatherResponses> call = client.getWeatherForMultipleCities(query, units, WEATHER_API_KEY);
        call.enqueue(new Callback<WeatherResponses>() {
            @Override
            public void onResponse(Call<WeatherResponses> call, Response<WeatherResponses> response) {
                if (response.isSuccessful()) {
                    List<WeatherResponse> weatherResponseList = new ArrayList<>();
                    WeatherResponses weatherResponses = response.body();
                    if (weatherResponses != null) {
                        weatherResponseList = weatherResponses.getList();
                    }
                    multipleCityListener.onSuccessResponseReturnedFromAPIMultiCityCall(weatherResponseList);
                }

            }

            @Override
            public void onFailure(Call<WeatherResponses> call, Throwable t) {

            }
        });

    }


}
