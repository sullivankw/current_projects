package com.apps.ksullivan.weatherapp;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.ksullivan.weatherapp.listener.WeatherApiGetByIdListener;
import com.apps.ksullivan.weatherapp.model.WeatherResponse;
import com.apps.ksullivan.weatherapp.service.CityServiceImpl;
import com.apps.ksullivan.weatherapp.service.WeatherServiceImpl;

/**
 * Created by ksullivan on 12/20/17.
 */

public class WeatherDetailFragment extends Fragment implements WeatherApiGetByIdListener {
    public static final String CITY_ID = "city_id";
    private CityServiceImpl cityService;
    private WeatherServiceImpl weatherService;
    private TextView cityName;
    private TextView humidity;
    private TextView pressure;
    private TextView currentT;
    private TextView minT;
    private TextView maxT;
    private WeatherResponse weatherResponse;
    private String cityId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        cityService = CityServiceImpl.getCityService(getContext());
        weatherService = new WeatherServiceImpl();
        weatherService.setWeatherApiGetByIdListener(this);

        if (getArguments() != null) {
            cityId = getArguments().getString(CITY_ID);
            getUpToDateWeatherDataForCity(cityId);
        }

    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.weather_detail_fragment, container, false);
        findAndSetViews(view);
        return view;
    }

    private void findAndSetViews(View view) {
        cityName = view.findViewById(R.id.cityNameDetail);
        humidity = view.findViewById(R.id.humidityDetail);
        pressure = view.findViewById(R.id.pressureDetail);
        currentT = view.findViewById(R.id.currentTemperatureDetail);
        minT = view.findViewById(R.id.minTDetail);
        maxT = view.findViewById(R.id.maxTDetail);

    }

    private void getUpToDateWeatherDataForCity(String cityId) {
        weatherService.getWeatherById(cityId);

    }

    public static WeatherDetailFragment newInstance(String cityId) {

        Bundle args = new Bundle();
        args.putString(CITY_ID, cityId);

        WeatherDetailFragment fragment = new WeatherDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onSuccessResponseReturnedFromAPIGetById(WeatherResponse weatherResponse) {

        if (weatherResponse != null) {
            cityName.setText(weatherResponse.getName());
            humidity.setText(String.valueOf(weatherResponse.getMain().getHumidity()));
            pressure.setText(String.valueOf(weatherResponse.getMain().getPressure()));
            currentT.setText(String.valueOf(correctKelvinToCelsius(weatherResponse.getMain().getTemp())));
            minT.setText(String.valueOf(correctKelvinToCelsius(weatherResponse.getMain().getTempMin())));
            maxT.setText(String.valueOf(correctKelvinToCelsius(weatherResponse.getMain().getTempMax())));
        }
    }

    private String correctKelvinToCelsius(double number) {
        String temp = String.valueOf(number - 273.15);
        if (temp.length() > 3) {
            temp = temp.substring(0, 4);
        }
        return temp;
    }
}
