package com.apps.ksullivan.weatherapp.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;


import com.apps.ksullivan.weatherapp.model.City;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;

/**
 * Created by ksullivan on 12/19/17.
 */

@Dao
public interface CityDao {

    @Query("SELECT * FROM city_table")
    List<City> getStoredCities();

    @Query("SELECT * FROM city_table WHERE city_id = :id")
    City getCityById(String id);

    //todo, not replacing....prolly id set issue
    @Insert(onConflict = REPLACE)
    void addCities(City... cities);

    @Delete
    void deleteBill(City city);
}
