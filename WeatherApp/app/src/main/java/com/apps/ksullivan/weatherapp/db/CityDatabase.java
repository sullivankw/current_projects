package com.apps.ksullivan.weatherapp.db;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.apps.ksullivan.weatherapp.model.City;

/**
 * Created by ksullivan on 12/19/17.
 */

@Database(entities = {City.class}, version = 1)
public abstract class CityDatabase extends RoomDatabase {

    private static CityDatabase INSTANCE;

    public abstract CityDao cityDao();

    public static CityDatabase getDatabase(Context c) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(c.getApplicationContext(), CityDatabase.class, "city_db")
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }
}
