package com.apps.ksullivan.weatherapp;

import android.content.Intent;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import com.apps.ksullivan.weatherapp.adaptor.MainActivityRecyclerViewAdaptor;
import com.apps.ksullivan.weatherapp.listener.FetchCityByIdListener;
import com.apps.ksullivan.weatherapp.listener.FetchCityListListener;
import com.apps.ksullivan.weatherapp.listener.RefreshListListener;
import com.apps.ksullivan.weatherapp.listener.WeatherApiMultipleCityListener;
import com.apps.ksullivan.weatherapp.model.City;
import com.apps.ksullivan.weatherapp.model.WeatherResponse;
import com.apps.ksullivan.weatherapp.service.CityServiceImpl;
import com.apps.ksullivan.weatherapp.service.WeatherServiceImpl;

import java.util.List;

public class MainActivity extends AppCompatActivity implements MainActivityRecyclerViewAdaptor.RecyclerViewItemOnClickListener, FetchCityListListener,
        WeatherApiMultipleCityListener, FetchCityByIdListener, RefreshListListener {
    private Button addButton;
    private RecyclerView recyclerView;
    private MainActivityRecyclerViewAdaptor recyclerViewAdaptor;
    private CityServiceImpl cityService;
    private WeatherServiceImpl weatherService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        configureToolbar();
        findViews();
        cityService = CityServiceImpl.getCityService(getApplicationContext());
        cityService.setFetchCityListListener(this);
        cityService.setFetchCityByIdListener(this);
        cityService.setRefreshListListener(this);
        weatherService = new WeatherServiceImpl();
        weatherService.setWeatherApiMultipleCityListener(this);
        configureRecyclerView();
        startGetListTask();

    }

    private void startGetListTask() {
        cityService.startGetCitiesTask();
    }


    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("GET WEATHER");
        setSupportActionBar(toolbar);
    }

    private void configureRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, 1);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerViewAdaptor = new MainActivityRecyclerViewAdaptor(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(recyclerViewAdaptor);
    }

    private void findViews() {
        addButton = findViewById(R.id.addCityBtn);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddCityActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public void onRecyclerViewItemClicked(WeatherResponse weatherResponse) {
        loadFragment(weatherResponse);
    }

    @Override
    public void onRecyclerViewItemClickedToDelete(WeatherResponse weatherResponse) {
        if (weatherResponse != null) {
            String cityId = String.valueOf(weatherResponse.getId());
            cityService.startGetCityByIdTask(cityId);
            //see onCityReturned(City city) for result
        }
    }

    private void loadFragment(WeatherResponse weatherResponse) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        WeatherDetailFragment fragment = WeatherDetailFragment.newInstance(String.valueOf(weatherResponse.getId()));
        fragmentTransaction.replace(R.id.fragmentContainer, fragment);
        fragmentTransaction.commit();
    }
    @Override
    public void onCityListFetched(List<City> cities) {
        if (cities != null) {
            weatherService.getWeatherForMultipleCities(cities);
        }
    }

    @Override
    public void onSuccessResponseReturnedFromAPIMultiCityCall(List<WeatherResponse> weatherResponseList) {
        if (weatherResponseList != null) {
            recyclerViewAdaptor.setItemList(weatherResponseList);
        }
        else {
            //present empty list
        }
    }

    @Override
    public void onCityReturned(City city) {
        //City has been returned that we want to delete
        if (city != null) {
            cityService.startDeleteCityTask(city);
        }
    }

    @Override
    public void onCityRefreshed() {
        finish();
        Intent i = new Intent(this, MainActivity.class);  //your class
        startActivity(i);

    }
}
