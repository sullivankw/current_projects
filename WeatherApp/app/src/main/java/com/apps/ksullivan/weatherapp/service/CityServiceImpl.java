package com.apps.ksullivan.weatherapp.service;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.apps.ksullivan.weatherapp.db.CityDatabase;
import com.apps.ksullivan.weatherapp.listener.AddCityListener;
import com.apps.ksullivan.weatherapp.listener.FetchCityByIdListener;
import com.apps.ksullivan.weatherapp.listener.FetchCityListListener;
import com.apps.ksullivan.weatherapp.listener.RefreshListListener;
import com.apps.ksullivan.weatherapp.model.City;

import java.util.List;

/**
 * Created by ksullivan on 12/19/17.
 */

public class CityServiceImpl {

    private static CityDatabase cityDatabase;
    private static CityServiceImpl INSTANCE;
    private AddCityListener listener;
    private FetchCityListListener fetchCityListListener;
    private FetchCityByIdListener fetchCityByIdListener;
    private RefreshListListener refreshListListener;

    public static CityServiceImpl getCityService(Context c) {
        if (INSTANCE == null) {
            INSTANCE = new CityServiceImpl();
        }
        createDataBase(c);

        return INSTANCE;
    }

    public static CityDatabase createDataBase(Context c) {
        cityDatabase = CityDatabase.getDatabase(c);
        return cityDatabase;
    }

    public void setAddCityListener( AddCityListener listener) {
        this.listener = listener;
    }

    public void setFetchCityListListener (FetchCityListListener fetchCityListListener) {
        this.fetchCityListListener = fetchCityListListener;
    }

    public void setFetchCityByIdListener(FetchCityByIdListener fetchCityByIdListener) {
        this.fetchCityByIdListener = fetchCityByIdListener;
    }

    public void setRefreshListListener(RefreshListListener refreshListListener) {
        this.refreshListListener = refreshListListener;
    }

    public void addCityToDataBase(City city) {
        new AddCityTask().execute(city);
    }


    private class AddCityTask extends AsyncTask<City, Void, Void> {

        @Override
        protected Void doInBackground(City... item) {

            cityDatabase.cityDao().addCities((item[0]));
            Log.i("saved", "city should be stored");
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            listener.onSavedCityComplete();
        }
    }

    public void startGetCitiesTask() {
        new GetCitiesTask().execute();
    }

    private class GetCitiesTask extends AsyncTask<Void, Void, List<City>> {

        @Override
        protected List<City> doInBackground(Void... voids) {
            return cityDatabase.cityDao().getStoredCities();
        }

        @Override
        protected void onPostExecute(List<City> cities) {
            //send cities to a main thread listener
            fetchCityListListener.onCityListFetched(cities);
        }
    }

    private class DeleteCityTask extends AsyncTask<City, Void, Void> {

        @Override
        protected Void doInBackground(City... item) {
            cityDatabase.cityDao().deleteBill(item[0]);
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            refreshListListener.onCityRefreshed();
        }
    }

    private class FetchCityByIdTask extends AsyncTask<String, Void, City> {

        @Override
        protected City doInBackground(String... strings) {
            return cityDatabase.cityDao().getCityById(strings[0]);
        }

        @Override
        protected void onPostExecute(City city) {
            super.onPostExecute(city);
            fetchCityByIdListener.onCityReturned(city);
        }
    }

    public void startGetCityByIdTask(String cityId) {
        new FetchCityByIdTask().execute(cityId);

    }

    public void startDeleteCityTask(City city) {
        new DeleteCityTask().execute(city);
    }
}
