package com.apps.ksullivan.weatherapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by ksullivan on 12/17/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class WeatherResponse {

    @JsonProperty("name")
    private String name;
    @JsonProperty("id")
    private long id;
    @JsonProperty("coord")
    private Coordinates coord;
    @JsonProperty("main")
    private Temperature main;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Coordinates getCoord() {
        return coord;
    }

    public Temperature getMain() {
        return main;
    }

    public void setMain(Temperature main) {
        this.main = main;
    }

    public void setCoord(Coordinates coord) {
        this.coord = coord;

    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Coordinates {

        private double lon;
        private double lat;

        public double getLon() {
            return lon;
        }

        public void setLon(double lon) {
            this.lon = lon;
        }

        public double getLat() {
            return lat;
        }

        public void setLat(double lat) {
            this.lat = lat;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    public class Temperature {
        @JsonProperty("temp")
        private double temp;
        private double pressure;
        private double humidity;
        @JsonProperty("temp_min")
        private double temp_min;
        @JsonProperty("temp_max")
        private double temp_max;

        public double getTemp() {
            return temp;
        }

        public double getPressure() {
            return pressure;
        }

        public double getHumidity() {
            return humidity;
        }

        public double getTempMin() {
            return temp_min;
        }

        public double getTempMax() {
            return temp_max;
        }
    }
}
