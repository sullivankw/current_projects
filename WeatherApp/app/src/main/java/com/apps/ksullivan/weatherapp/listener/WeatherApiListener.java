package com.apps.ksullivan.weatherapp.listener;

import com.apps.ksullivan.weatherapp.model.WeatherResponse;

/**
 * Created by ksullivan on 12/19/17.
 */

public interface WeatherApiListener {

    void onSuccessResponseReturnedFromAPI(WeatherResponse body);
}
