package com.apps.ksullivan.weatherapp.adaptor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.apps.ksullivan.weatherapp.R;
import com.apps.ksullivan.weatherapp.model.WeatherResponse;

import java.util.List;

/**
 * Created by ksullivan on 12/18/17.
 */

public class MainActivityRecyclerViewAdaptor extends RecyclerView.Adapter<MainActivityRecyclerViewAdaptor.ListItemViewHolder> {
    private RecyclerViewItemOnClickListener listener;
    private List<WeatherResponse> weatherResponses;

    public MainActivityRecyclerViewAdaptor(RecyclerViewItemOnClickListener listener) {
        this.listener = listener;
    }

    public interface RecyclerViewItemOnClickListener {
        void onRecyclerViewItemClicked(WeatherResponse weatherResponse);
        //gotta convert to a city on the callback
        void onRecyclerViewItemClickedToDelete(WeatherResponse weatherResponse);
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());

        return new ListItemViewHolder(inflater.inflate(R.layout.list_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        WeatherResponse weatherResponse = weatherResponses.get(holder.getAdapterPosition());
        holder.getCityName().setText(weatherResponse.getName());
        holder.getTemperature().setText(String.valueOf(weatherResponse.getMain().getTemp() + " C"));

        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //inform the listening main activity will use to transact a fragment detail
                listener.onRecyclerViewItemClicked(weatherResponses.get(holder.getAdapterPosition()));

            }
        });

        holder.getDeleteButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onRecyclerViewItemClickedToDelete(weatherResponses.get(holder.getAdapterPosition()));

            }
        });

    }

    public void setItemList(List<WeatherResponse> responses) {
        this.weatherResponses = responses;
        ///recycler view method
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (weatherResponses == null) {
            return 0;
        }
        return weatherResponses.size();
    }

    public class ListItemViewHolder extends RecyclerView.ViewHolder {
        private TextView cityName;
        private TextView temperature;
        private Button deleteButton;

        public ListItemViewHolder(View itemView) {
            super(itemView);
            cityName = itemView.findViewById(R.id.cityName);
            temperature = itemView.findViewById(R.id.currentTemperature);
            deleteButton = itemView.findViewById(R.id.deleteButton);
        }

        public View getItemView() {
            return itemView;
        }

        public TextView getCityName() {
            return cityName;
        }

        public void setCityName(TextView cityName) {
            this.cityName = cityName;
        }

        public TextView getTemperature() {
            return temperature;
        }

        public void setTemperature(TextView temperature) {
            this.temperature = temperature;
        }

        public Button getDeleteButton() {
            return deleteButton;
        }

        public void setDeleteButton(Button deleteButton) {
            this.deleteButton = deleteButton;
        }
    }
}
