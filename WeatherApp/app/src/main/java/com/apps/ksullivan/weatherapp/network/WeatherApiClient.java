package com.apps.ksullivan.weatherapp.network;

import com.apps.ksullivan.weatherapp.model.WeatherResponse;
import com.apps.ksullivan.weatherapp.model.WeatherResponses;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by ksullivan on 12/19/17.
 */

public interface WeatherApiClient {

    //@GET("weather?q={name}&APPID=3158c872759587a0e6e876b634e839f4")
    @GET("weather")
    Call<WeatherResponse> getWeatherByCityName(@Query("q") String name, @Query("APPID") String key);

    //api.openweathermap.org/data/2.5/weather?zip=94040,us
    @GET("weather")
    Call<WeatherResponse> getWeatherByZip(@Query("zip") String zipAndCountryCode, @Query("APPID") String key);

    //api.openweathermap.org/data/2.5/weather?lat=35&lon=139
    @GET("weather")
    Call<WeatherResponse> getWeatherByCoordinates(@Query("lat") String lat, @Query("lon") String lon, @Query("APPID") String key);

    //http://api.openweathermap.org/data/2.5/group?id=524901,703448,2643743&units=metric
    ///data/2.5/group?id=524901,703448,2643743&units=metric&APPID={APIKEY}
    @GET("group")
    Call<WeatherResponses> getWeatherForMultipleCities(@Query("id") String ids, @Query("units") String metric, @Query("APPID") String key);

    //api.openweathermap.org/data/2.5/weather?id=2172797
    @GET("weather")
    Call<WeatherResponse> getWeatherById(@Query("id") String id, @Query("APPID") String key);

}
