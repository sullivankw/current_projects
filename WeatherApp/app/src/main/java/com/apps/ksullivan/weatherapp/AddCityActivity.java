package com.apps.ksullivan.weatherapp;

import android.Manifest;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.apps.ksullivan.weatherapp.listener.AddCityListener;
import com.apps.ksullivan.weatherapp.listener.WeatherApiListener;
import com.apps.ksullivan.weatherapp.model.City;
import com.apps.ksullivan.weatherapp.model.WeatherResponse;
import com.apps.ksullivan.weatherapp.service.CityServiceImpl;
import com.apps.ksullivan.weatherapp.service.WeatherServiceImpl;
import com.apps.ksullivan.weatherapp.utility.LocationTracker;

/**
 * Created by ksullivan on 12/19/17.
 */

public class AddCityActivity extends AppCompatActivity implements WeatherApiListener, AddCityListener {
    private TextInputEditText cityInput;
    private TextInputLayout cityInputLayout;
    private TextInputEditText zipInput;
    private TextInputLayout zipInputLayout;
    private boolean isOk;
    private CardView cardViewCity;
    private CardView cardViewZip;
    private CardView cardViewLocation;
    private WeatherServiceImpl weatherService;
    private CityServiceImpl cityService;
    private LocationTracker locationTracker;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_city);
        findViews();
        configureCardViews();
        configureServiceClasses();
        configureToolbar();
        ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 111);
        Toast.makeText(this, "Enter a field and tap the box to get weather.", Toast.LENGTH_SHORT ).show();
    }

    private void configureToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("ADD CITY");
        setSupportActionBar(toolbar);
    }

    private Location getUserLocation() {
        locationTracker = new LocationTracker(getApplicationContext());
        Location location = locationTracker.getLocation();
        if (location != null) {
            Log.i("Long", String.valueOf(location.getLongitude()));
            Log.i("Lat", String.valueOf(location.getLatitude()));
        }
        return location;
    }

    private void configureServiceClasses() {
        weatherService = new WeatherServiceImpl();
        weatherService.setWeatherApiListener(this);
        cityService = CityServiceImpl.getCityService(getApplicationContext());
        cityService.setAddCityListener(this);
    }

    public void findViews() {
        cityInput = findViewById(R.id.inputCity);
        zipInput = findViewById(R.id.inputZip);
        zipInputLayout = findViewById(R.id.zipInputLayout);
        cityInputLayout = findViewById(R.id.cityInputLayout);
        cardViewCity = findViewById(R.id.cardViewCity);
        cardViewLocation = findViewById(R.id.cardViewLocation);
        cardViewZip = findViewById(R.id.cardViewZip);

    }

    private void configureCardViews() {
        cardViewCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weatherService.getWeatherByCity(cityInput.getText().toString());
            }
        });

        cardViewZip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                weatherService.getWeatherByZip(zipInput.getText().toString());
            }
        });

        cardViewLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //location services
                String longitude = null;
                String latitude = null;
                Location location =getUserLocation();
                if (location != null) {
                    latitude = String.valueOf(location.getLatitude());
                    longitude = String.valueOf(location.getLongitude());
                    weatherService.getWeatherByLocation(latitude,longitude);
                } else {
                    Toast.makeText(AddCityActivity.this, "No location available", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

    @Override
    public void onSuccessResponseReturnedFromAPI(WeatherResponse response) {
        //take response object and build a city object to store into my db
        if (response == null) {
            Toast.makeText(this, "City Not Found", Toast.LENGTH_SHORT).show();
        }

        if (response != null) {
            City city = new City();
            city.setName(response.getName());
            city.setCityId(String.valueOf(response.getId()));
            city.setTemp(String.valueOf(response.getMain().getTemp()));
            cityService.addCityToDataBase(city);
            //wait for onSavedCityComplete()
        }
    }

    @Override
    public void onSavedCityComplete() {
        Toast.makeText(this, "City Added", Toast.LENGTH_SHORT).show();
        Intent i = new Intent(AddCityActivity.this, MainActivity.class);
        startActivity(i);

    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("resumeCalled", "The resume method was called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("restartCalled", "The restart method was called");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("pauseCalled", "The pause method was called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("destroyCalled", "The destroy method was called");
    }
}
