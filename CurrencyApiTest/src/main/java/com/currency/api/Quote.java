package com.currency.api;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;



@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
	@JsonProperty("USDAED")
	private Double USDAED;
	@JsonProperty("USDAFN")
	private Double USDAFN;
	@JsonProperty("USDALL")
	private Double USDALL;
	@JsonProperty("USDAMD")
	private Double USDAMD;
	@JsonProperty("USDANG")
	private Double USDANG;
	@JsonProperty("USDAOA")
	private Double USDAOA;
	@JsonProperty("USDARS")
	private Double USDARS;
	@JsonProperty("USDAUD")
	private Double USDAUD;
	@JsonProperty("USDAWG")
	private Double USDAWG;
	@JsonProperty("USDAZN")
	private Double USDAZN;
	@JsonProperty("USDBAM")
	private Double USDBAM;
	
	
	
	
	
	
	
	public Double getUSDAMD() {
		return USDAMD;
	}
	public void setUSDAMD(Double uSDAMD) {
		USDAMD = uSDAMD;
	}
	public Double getUSDANG() {
		return USDANG;
	}
	public void setUSDANG(Double uSDANG) {
		USDANG = uSDANG;
	}
	public Double getUSDAOA() {
		return USDAOA;
	}
	public void setUSDAOA(Double uSDAOA) {
		USDAOA = uSDAOA;
	}
	public Double getUSDARS() {
		return USDARS;
	}
	public void setUSDARS(Double uSDARS) {
		USDARS = uSDARS;
	}
	public Double getUSDAUD() {
		return USDAUD;
	}
	public void setUSDAUD(Double uSDAUD) {
		USDAUD = uSDAUD;
	}
	public Double getUSDAWG() {
		return USDAWG;
	}
	public void setUSDAWG(Double uSDAWG) {
		USDAWG = uSDAWG;
	}
	public Double getUSDAZN() {
		return USDAZN;
	}
	public void setUSDAZN(Double uSDAZN) {
		USDAZN = uSDAZN;
	}
	public Double getUSDBAM() {
		return USDBAM;
	}
	public void setUSDBAM(Double uSDBAM) {
		USDBAM = uSDBAM;
	}
	public Double getUSDAED() {
		return USDAED;
	}
	public void setUSDAED(Double uSDAED) {
		USDAED = uSDAED;
	}
	public Double getUSDAFN() {
		return USDAFN;
	}
	public void setUSDAFN(Double uSDAFN) {
		USDAFN = uSDAFN;
	}
	public Double getUSDALL() {
		return USDALL;
	}
	public void setUSDALL(Double uSDALL) {
		USDALL = uSDALL;
	}

}
