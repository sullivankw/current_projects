package com.currency.api;



import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.boot.CommandLineRunner;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.ClientHttpRequestFactory;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import junit.framework.Assert;

@Component
public class CurrencyService implements CommandLineRunner{
	
	private final Logger logger = Logger.getLogger(this.getClass() );
	
	public static final String URL ="http://www.apilayer.net/api/live?access_key=1ef59f8590a7974c02215ba41c8ec736";
	
	//Most likely error codes returned
	
	public static final String NOT_FOUND = "404";
	
	public static final String ACCESS_KEY_ERROR = "101";
	
	public static final String BAD_REQUEST = "103";
	
	public static final String EXCEEDED_REQUESTS_ALLOWED = "104";
	
	public static final String ACCOUNT_NOT_SUPPORT_REQUEST = "105";
	
	public static final String INACTIVE_ACCOUNT = "102";
	
	public static final String INVALID_SOURCE_CURRENCY = "201";
	
	public static final String INVALID_AMOUNT = "105";
	
	
	
	
	private void getCurrencyResponse() throws Exception {
	
		RestTemplate temp = new RestTemplate();
		
		HttpEntity<CurrencyResponse> entityResponse = null;
		try {
			entityResponse = temp.getForEntity(URL, CurrencyResponse.class);	
			if (entityResponse !=null) {
				System.out.println("Request Successful");
				System.out.println("The below exchange rates are relative to the currency: " + entityResponse.getBody().getSource());			
				System.out.println("Current exchange rate for AMD: " + entityResponse.getBody().getQuotes().getUSDAMD() + " at timestamp " +
						entityResponse.getBody().getTimestamp());
				System.out.println("-------------------");
				System.out.println("Current exchange rate for AOA: " + entityResponse.getBody().getQuotes().getUSDAOA()+ " at timestamp " +
						entityResponse.getBody().getTimestamp());
				
			}			
			//Note not catching exceptions...will throw to a client to handle when used in Android		
		} catch(Exception e) {
			if (e instanceof HttpClientErrorException) {
				translateCurrencyApiException(e);			
			} else {
				sendErrorDebugMessage("no code");
				throw new Exception("Unable to handle request at this time");
			}
			
		}
				
	}
	
	private void translateCurrencyApiException(Exception e) throws Exception {
		if (((HttpClientErrorException) e).getStatusCode().toString().equals(NOT_FOUND)) {
			sendErrorDebugMessage(((HttpClientErrorException) e).getStatusCode().toString());
			throw new Exception("Can't locate web service");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(ACCESS_KEY_ERROR)) {
			throw new Exception("Access key doesn't match records");
		} else if  (((HttpClientErrorException) e).getStatusCode().toString().equals(BAD_REQUEST)) {
			throw new Exception("Can't perform operation");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(EXCEEDED_REQUESTS_ALLOWED)) {
			throw new Exception("Too many requests this month");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(ACCOUNT_NOT_SUPPORT_REQUEST)) {
			throw new Exception("Upgrade your subscription to access this request");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(INACTIVE_ACCOUNT )) {
			throw new Exception("Your account has been closed due to inactivity");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(INVALID_SOURCE_CURRENCY)) {
			throw new Exception("Please check your source currency");
		} else if (((HttpClientErrorException) e).getStatusCode().toString().equals(INVALID_AMOUNT)) {
			throw new Exception("Please check your amount entered");
		} else {
			throw new Exception("Unable to call currency api");
		}
		
	}
	//TODO make relevant message
	private void sendErrorDebugMessage(String errorCode){
	
		if (logger.isDebugEnabled()) {
			logger.debug("Problem calling the api with response error code " + errorCode);
		}
	}

	@Override
	public void run(String... arg0) throws Exception {
		Map<String, String> myMap = new HashMap<String, String>();
		
		myMap.put("1", "one");
		myMap.put("2", "two");
		String gotit = myMap.get("1");
		String gotit2 = myMap.get("3");
		System.out.println("here is da map!!!!!!!!!" + gotit);
		System.out.println("here is da map!!!!!!!!!" + gotit2);
		this.getCurrencyResponse();
		
	}
	


}
