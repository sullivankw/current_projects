package com.apps.ksullivan.payyobills;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.apps.ksullivan.payyobills.db.Bill;

import java.util.List;

/**
 * Created by ksullivan on 10/2/17.
 */

public class BillRecyclerViewAdaptor extends RecyclerView.Adapter<BillRecyclerViewAdaptor.BillViewHolder> {

    private BillOnClickListener listener;
    private List<Bill> bills;

    public BillRecyclerViewAdaptor(BillOnClickListener listener) {
        this.listener = listener;
    }

    public interface BillOnClickListener {
        void onBillClicked(Bill bill);
    }

    @Override
    public BillViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());

        return new BillViewHolder(inflater.inflate(R.layout.bill_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final BillViewHolder holder, int position) {

        Bill bill = bills.get(holder.getAdapterPosition());


        holder.getAmountLabel().setText("Amount");
        holder.getCompanyNameLabel().setText("Company");

        holder.getAmount().setText(bill.getAmount());
        holder.getCompanyName().setText(bill.getCompanyName());

        //todo make name more generic since we can pass in multi types
        if (bill.isPaid()) {
            holder.getDueDate().setText(bill.getPaidDate());
            holder.getDueDateLabel().setText("Paid Date");
        } else {
            holder.getDueDateLabel().setText("Due Date");
            holder.getDueDate().setText(bill.getDueDate());
        }

        holder.getItemView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onBillClicked(bills.get(holder.getAdapterPosition()));
            }
        });

    }
    //this is the approach that i find odd, need to check other view model impls
    //also the mutherfucker is blacnk until this is called
    public void setBillsList(List<Bill> bills) {
        this.bills = bills;
        ///recycler view method
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (bills == null) {
            return 0;
        } else
            return bills.size();

    }

    public class BillViewHolder extends RecyclerView.ViewHolder {
        //will expand
        TextView amountLabel;
        TextView amount;
        TextView companyNameLabel;
        TextView companyName;
        TextView dueDateLabel;
        TextView dueDate;


        public BillViewHolder(View itemView) {
            super(itemView);
            amount = itemView.findViewById(R.id.amount);
            amountLabel = itemView.findViewById(R.id.amountLabel);
            companyName = itemView.findViewById(R.id.companyName);
            companyNameLabel = itemView.findViewById(R.id.companyNameLabel);
            dueDate = itemView.findViewById(R.id.dueDate);
            dueDateLabel = itemView.findViewById(R.id.dueDateLabel);
        }

        public TextView getAmountLabel() {
            return amountLabel;
        }

        public TextView getCompanyNameLabel() {
            return companyNameLabel;
        }

        public TextView getDueDateLabel() {
            return dueDateLabel;
        }

        public TextView getDueDate() {
            return dueDate;
        }

        public View getItemView() {
            return itemView;
        }

        public TextView getAmount() {
            return amount;
        }

        public TextView getCompanyName() {
            return companyName;
        }


    }
}
