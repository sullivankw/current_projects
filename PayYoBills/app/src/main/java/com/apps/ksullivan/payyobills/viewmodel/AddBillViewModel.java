package com.apps.ksullivan.payyobills.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.apps.ksullivan.payyobills.BillDaoImpl;
import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.BillDatabase;


/**
 * Created by ksullivan on 10/2/17.
 */

public class AddBillViewModel extends AndroidViewModel {

    private String amount;
    private String companyName;
    private String month;
    private BillDatabase database;
    private MutableLiveData<Bill> bill;
    private Boolean monthInputChecker;
    private Boolean amountInputChecker;
    private Boolean companyInputChecker;
    private String dueDate;


    public AddBillViewModel(Application application) {
        super(application);
        //do i need to create the dbs in these constructors
        database = BillDaoImpl.getBillService().createDataBase(application);
    }

    //should be an observable????
    public Boolean getShouldEnableNext() {
        if (getMonthInputChecker() && getAmountInputChecker() && getCompanyInputChecker()) {
            return true;
        } else
            return false;
    }

    public Boolean getMonthInputChecker() {
//        if (monthInputChecker == null) {
//            monthInputChecker = false;
//        }

        monthInputChecker = true;

        return monthInputChecker;
    }

    public void setMonthInputChecker(Boolean monthInputChecker) {
        this.monthInputChecker = monthInputChecker;
    }

    public Boolean getAmountInputChecker() {
        if (amountInputChecker == null) {
            amountInputChecker = false;
        }
        return amountInputChecker;
    }

    public void setAmountInputChecker(Boolean amountInputChecker) {

        this.amountInputChecker = amountInputChecker;
    }

    public Boolean getCompanyInputChecker() {
        if (companyInputChecker == null) {
            companyInputChecker = false;
        }
        return companyInputChecker;
    }

    public void setCompanyInputChecker(Boolean companynputChecker) {
        this.companyInputChecker = companynputChecker;
    }

    public LiveData<Bill> getBill() {
        if (bill == null) {
            bill = new MutableLiveData<>();
            bill.setValue(null);
        }
        if (month != null && companyName != null && amount != null)  {
            bill.setValue(new Bill(amount, companyName, dueDate));
        }
        return bill;
    }

    public void setBill(MutableLiveData<Bill> bill) {
        this.bill = bill;

    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }


    public void addBill(Bill bill){
        new AddBillTask().execute(bill);
    }

    private class AddBillTask extends AsyncTask<Bill, Void, Void> {

        @Override
        protected Void doInBackground(Bill... item) {
            BillDaoImpl.getBillService().addBill((item[0]));
            return null;
        }
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }
}
