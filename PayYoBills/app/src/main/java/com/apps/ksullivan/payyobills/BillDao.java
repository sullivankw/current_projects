package com.apps.ksullivan.payyobills;

import android.arch.lifecycle.LiveData;

import com.apps.ksullivan.payyobills.db.Bill;

import java.util.List;

/**
 * Created by ksullivan on 10/4/17.
 */

interface BillDao {

    LiveData<List<Bill>> getUnpaidBills();

    LiveData<List<Bill>> getPaidBills();

    void addBill(Bill bill);

    void deleteBill(Bill bill);


}
