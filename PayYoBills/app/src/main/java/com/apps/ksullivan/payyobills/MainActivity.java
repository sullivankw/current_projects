package com.apps.ksullivan.payyobills;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.Cow;
import com.apps.ksullivan.payyobills.db.GsonBills;
import com.apps.ksullivan.payyobills.viewmodel.BillViewModel;
import com.google.gson.Gson;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MainActivity extends BaseBillActivity implements BillRecyclerViewAdaptor.BillOnClickListener {

    private static final String BILL_DETAIL = "Bill_Detail";
    private BillRecyclerViewAdaptor adaptor;
    private BillViewModel viewModel;
    private RecyclerView recyclerView;
    private Button button;
    private List<Bill> localBills;

    //Job Scheduling params
    private static final int JOB_ID = 44;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("BILLS TO PAY");
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        button = (Button) findViewById(R.id.button);
                button.setText("ADD BILL");

        configureRecyclerView();
        configureViewModel();
        configureButton();
    }

    private void scheduleJob() {

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);

        if (jobScheduler != null) {
            for (JobInfo jobInfo : jobScheduler.getAllPendingJobs()) {
                if (jobInfo.getId() == JOB_ID) {
                    //cancel previous job if already scheduled
                    jobScheduler.cancel(JOB_ID);
                }
            }
        }
        //Here we are saving to the device the list of current bills. When the job runs once a day it will check if a bill is due
        //within 24 hours. If so, it will generate a notification
        GsonBills gsonBills = new GsonBills();
        gsonBills.setBills(localBills);
        Gson g = new Gson();

        String json = g.toJson(gsonBills);

        PersistableBundle bundle = new PersistableBundle();
        bundle.putString("GSONBillsAsJSON", json);

    JobInfo jobInfo = new JobInfo.Builder(JOB_ID, new ComponentName(getApplicationContext(), BillJobService.class))
            //todo will change to every 24 hours
            .setPeriodic(1000 * 60 * 60 * 500)
           // .setPeriodic(TimeUnit.MINUTES.toMillis(5))
           .setExtras(bundle)

            //.setPeriodic(TimeUnit.MINUTES.toMillis(20), TimeUnit.MINUTES.toMillis(60))
            //.setOverrideDeadline(1000 * 10)
           //.setPersisted(true)
            .build();
             jobScheduler.schedule(jobInfo);
        int resultCode = jobScheduler.schedule(jobInfo);
        if (resultCode == JobScheduler.RESULT_SUCCESS) {
            Log.i("hi", "Job scheduled: " + jobInfo.getService() + " at " + jobInfo.getIntervalMillis());
        } else {
            Log.i("hi", "Job not scheduled");
        }

    }
    private void isScheduled() {
        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        for (JobInfo jobInfo: jobScheduler.getAllPendingJobs()) {
            System.out.println("job service: " + jobInfo.getService());
            System.out.println("job id: " + jobInfo.getId());
        }

    }

    private void configureButton() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(MainActivity.this, AddBillActivity.class);
                NotificationGenerator.createNotifification(
                        "Here's your update", "Title!", "body", R.drawable.ic_launcher, getApplicationContext());
                startActivity(i);

            }
        });

    }

    private void configureRecyclerView() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, 1);
        recyclerView.addItemDecoration(dividerItemDecoration);
        adaptor = new BillRecyclerViewAdaptor(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(adaptor);


    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this).get(BillViewModel.class);
        viewModel.getBills().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                if (bills != null) {
                    //note that my bills are null even after calling back to my db until this setbillslist is
                    //called
                    localBills = bills;
                    adaptor.setBillsList(bills);

                    //i may need to call these each....the list im saving in the job changes if the list does
                    //tho this cant be optimal

                    scheduleJob();
                   // isScheduled();

                }

            }
        });

    }

    @Override
    public void onBillClicked(Bill bill) {

        Intent i = new Intent(this, BillPagerActivity.class);
        i.putExtra("bill_id", bill.getId());
        //multiple activities are using this detail view...we need to id which one is calling so to display
        //the correct xml
        i.putExtra("activity_id", "MainActivity");
        startActivity(i);
    }


}
