package com.apps.ksullivan.payyobills;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.viewmodel.BillDetailViewModel;

/**
 * Created by ksullivan on 10/8/17.
 */
public class BillDetailActivity extends AppCompatActivity  {
    private TextView detailCompany;
    private TextView detailAmount;
    private  TextView detailDate;
    private Button button;
    private Button backButton;
    private BillDetailViewModel viewModel;
    private Bill bill;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_detail);
        findViews();
        bill = (Bill) getIntent().getSerializableExtra("bill");
        setViews(bill);
        configureViewModel();
        setButton();
        setBackButton();

    }

    private void setBackButton() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(BillDetailActivity.this, MainActivity.class);
                startActivity(i);
            }
        });

    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this).get(BillDetailViewModel.class);
        //todo i dont think i need to observe the bill since it is not editable
    }

    private void findViews() {
        detailDate = (TextView) findViewById(R.id.detailDate);
        detailCompany = (TextView) findViewById(R.id.detailCopmany);
        detailAmount = (TextView) findViewById(R.id.detailAmount);
        button = (Button) findViewById(R.id.deleteButton);
        backButton = (Button) findViewById(R.id.backButton);
    }

    private void setViews(Bill bill) {
        detailDate.setText(bill.getDueDate());
        detailCompany.setText(bill.getCompanyName());
        detailAmount.setText(bill.getAmount());
    }

    private void setButton() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo popup dialog to confirm deleting
                viewModel.deleteBill(bill);
                Intent i = new Intent(BillDetailActivity.this, MainActivity.class);
                startActivity(i);

            }
        });
    }

}
