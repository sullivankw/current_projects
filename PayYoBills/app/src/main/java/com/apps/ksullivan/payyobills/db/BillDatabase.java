package com.apps.ksullivan.payyobills.db;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.migration.Migration;
import android.content.Context;

/**
 * Created by ksullivan on 10/2/17.
 */

@Database(entities = {Bill.class}, version = 6)
//A migration from 1 to 2 is necessary. Please provide a Migration in the builder or call fallbackToDestructiveMigration in the builder in which case Room will re-create all of the tables.
public abstract class BillDatabase extends RoomDatabase {

    private static BillDatabase INSTANCE;

    public abstract BillDao billDao();

    public static BillDatabase getDatabase(Context c) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(c.getApplicationContext(), BillDatabase.class, "bill_db")
                    //.addMigrations(MIGRATION_1_2)
                    .fallbackToDestructiveMigration()
                    .build();
        }
        return INSTANCE;
    }

    public static void destroyInstance() {
        INSTANCE = null;
    }

    static final Migration MIGRATION_1_2 = new Migration(2, 3) {
        @Override
        public void migrate(SupportSQLiteDatabase database) {
            database.execSQL("CREATE TABLE bill_table ");

        }
    };

}
