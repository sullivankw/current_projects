package com.apps.ksullivan.payyobills;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;

import static android.content.Context.NOTIFICATION_SERVICE;

/**
 * Created by ksullivan on 11/13/17.
 */

public class NotificationGenerator {

    public static void createNotifification(String ticker, String title, String body, int icon, Context context) {
        NotificationCompat.Builder notification =  new NotificationCompat.Builder(context);
        notification.setAutoCancel(true);
        notification.setSmallIcon(icon);
        notification.setTicker(ticker);
        notification.setWhen(System.currentTimeMillis());
        notification.setContentTitle(title);
        notification.setContentText(body);

        //notification.setExtras();
        //potentially could send the bill id which would be returned to me
        //i could use it to start the

        //when we click the notification opens app to here
        Intent intent = new Intent(context, MainActivity.class);

        // The stack builder object will contain an artificial back stack for the
// started Activity.
// This ensures that navigating backward from the Activity leads out of
// your app to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
// Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
// Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(intent);

        PendingIntent pendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
        notification.setContentIntent(pendingIntent);

        //this guy can send out notifications
//        NotificationManagerCompat n = NotificationManagerCompat.from(context);
//        n.notify(0, no);
        NotificationManager nm = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
        nm.notify(567574, notification.build());

    }
}
