package com.apps.ksullivan.payyobills.db;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import com.apps.ksullivan.payyobills.db.Bill;

import java.util.List;

import static android.arch.persistence.room.OnConflictStrategy.REPLACE;


/**
 * Created by ksullivan on 10/2/17.
 */

@Dao
public interface BillDao {

    @Query("SELECT * FROM bill_table WHERE paid = :falseBoolean")
    LiveData<List<Bill>> getBills(boolean falseBoolean);

    //i could just do my own filtering
    @Query("SELECT * FROM bill_table WHERE paid = :trueBoolean")
    LiveData<List<Bill>> getPaidBills(boolean trueBoolean);

    //todo how to pass in that param
    @Query("SELECT * FROM bill_table WHERE id = :id")
    LiveData<Bill> getBill(int id);

    //has to return a rowId
    @Insert(onConflict = REPLACE)
    void addBills(Bill... bills);

    @Delete
    void deleteBill(Bill bill);
}
