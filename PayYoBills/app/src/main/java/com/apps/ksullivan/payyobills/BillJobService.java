package com.apps.ksullivan.payyobills;

import android.app.job.JobInfo;
import android.app.job.JobParameters;
import android.app.job.JobScheduler;
import android.app.job.JobService;
import android.content.ComponentName;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.Cow;
import com.apps.ksullivan.payyobills.db.GsonBills;
import com.google.gson.Gson;

import java.io.FileOutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by ksullivan on 11/12/17.
 */

public class BillJobService extends JobService {
    private BillJobTask billJobTask;

    @Override
    public boolean onStartJob(JobParameters jobParameters) {
         billJobTask = new BillJobTask();
        billJobTask.execute(jobParameters);
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        //if job is interupted we check cancel it so it can start again
        if (billJobTask != null) {
            billJobTask.cancel(true);
        }
        //this boolean means it will be resched as it wasnt completed
        return true;
    }

    private class BillJobTask extends AsyncTask<JobParameters, Void, Void> {

        @Override
        protected Void doInBackground(JobParameters... jobParameters) {
            JobParameters jobParam = jobParameters[0];
            String json = jobParam.getExtras().getString("GSONBillsAsJSON");
            Gson g = new Gson();
            GsonBills gsonBills = g.fromJson(json, GsonBills.class);

            Date tomorrow = Calendar.getInstance().getTime();
            tomorrow.setHours(+24);
            SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
            String tomorrowString = dateFormat.format(tomorrow);

            if (gsonBills.getBills() != null) {
                for (Bill bill : gsonBills.getBills()) {
                    if (bill.getDueDate().equals(tomorrowString)) {
                        //certain that it will return 2 diff types
                        NotificationGenerator.createNotifification(
                                "Here's your update", "Bill Due Tomorrow", "Your bill is due tomorrow for " + bill.getAmount()
                                        + " to " + bill.getCompanyName(), R.drawable.ic_launcher, getApplicationContext());
                    }
                }
            }
            //if it completes
            jobFinished(jobParam, false);
            return null;
        }
    }

}
