package com.apps.ksullivan.payyobills.db;

import java.io.Serializable;

/**
 * Created by ksullivan on 11/13/17.
 */

public class Cow implements Serializable {

    private String name;
    private String size;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSize() {
        return size;
    }

    public void setSize(String size) {
        this.size = size;
    }
}
