package com.apps.ksullivan.payyobills.db;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.Nullable;

import java.io.Serializable;
import java.util.Date;
import java.util.UUID;

/**
 * Created by ksullivan on 7/9/17.
 */

@Entity(tableName = "bill_table")
public class Bill implements Serializable {

    private static final long serialVersionUID = 42L;

    @PrimaryKey(autoGenerate = true)
    private int id;
    @ColumnInfo(name = "paid")
    @Nullable
    private boolean paid;
    @ColumnInfo(name = "amount")
    private String amount;
    @ColumnInfo(name = "company_name")
    private String companyName;
    @ColumnInfo(name = "due_date")
    private String dueDate;
    @Nullable
    @ColumnInfo(name = "paid_date")
    private String paidDate;



    public Bill(String amount, String companyName, String dueDate) {
        this.amount = amount;
        this.companyName = companyName;
        this.dueDate = dueDate;
    }

    public int getId() {
        return id;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    @Nullable
    public boolean isPaid() {
        return paid;
    }

    public void setPaid(@Nullable boolean paid) {
        this.paid = paid;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    @Nullable
    public String getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(@Nullable String paidDate) {
        this.paidDate = paidDate;
    }
}
