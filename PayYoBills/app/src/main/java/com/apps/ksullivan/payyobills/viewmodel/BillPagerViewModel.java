package com.apps.ksullivan.payyobills.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.apps.ksullivan.payyobills.BillDaoImpl;
import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.BillDatabase;

import java.util.List;

/**
 * Created by ksullivan on 10/13/17.
 */

public class BillPagerViewModel extends AndroidViewModel {

    private LiveData<List<Bill>> bills;
    private BillDatabase database;
    private String callingActivity;

    public BillPagerViewModel(Application application) {
        super(application);
        database = BillDaoImpl.getBillService().createDataBase(application);
    }

    public LiveData<List<Bill>> getBills() {
        //todo convert to enum
        if (bills == null) {
            bills = new MutableLiveData<>();

            switch (getCallingActivity()) {
                case "MainActivity":
                    bills = BillDaoImpl.getBillService().getUnpaidBills();
                    break;
                case "PaidBillsActivity":
                    bills = BillDaoImpl.getBillService().getPaidBills();
                    break;
            }
        }

        return bills;
    }

    public String getCallingActivity() {
        return callingActivity;
    }

    public void setCallingActivity(String callingActivity) {
        this.callingActivity = callingActivity;
    }
}
