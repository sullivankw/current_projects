package com.apps.ksullivan.payyobills;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.viewmodel.AddBillViewModel;

import java.time.LocalDate;
import java.util.Calendar;

/**
 * Created by ksullivan on 10/5/17.
 */

public class AddBillActivity extends AppCompatActivity {

    private TextInputEditText amount;
    private TextInputLayout amountLayout;
    private TextInputEditText company;
    private TextInputLayout companyLayout;
    private Button submitButton;
    private AddBillViewModel addBillVewModel;
    private Boolean shouldEnableNext;

    //datepicker variables
    private int year_x, month_x, day_x;
    static final int DIALOG_ID = 5;
    private TextView clickableDate;
    private CardView cardView;
    private String dueDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_bill);
        findViews();
        setCurrentDate();
        setClickableCardView();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("ADD A BILL");
        setSupportActionBar(toolbar);
        //configureMonthEditText();
        configureViewModel();
        configureAmountEditText();
        configureCompanyNameEditText();
        configureAddBillListener();

    }


    private void setClickableCardView() {
        clickableDate.setText((month_x+1) + "-" + day_x + "-" + year_x);
        dueDate = dueDate = String.valueOf(month_x) + "-" + String.valueOf(day_x) + "-" + String.valueOf(year_x);
        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID);
            }
        });

        //also have to set the textview as clickable
        clickableDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(DIALOG_ID);
            }
        });



    }

    private void setCurrentDate() {
        Calendar cal = Calendar.getInstance();
        day_x = cal.get(Calendar.DAY_OF_MONTH);
        month_x = cal.get(Calendar.MONTH);
        year_x = cal.get(Calendar.YEAR);

    }

    @Override
    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_ID) {
            return new DatePickerDialog(this, datePickerListener, year_x, month_x, day_x);
        }
        return null;
    }

    private DatePickerDialog.OnDateSetListener datePickerListener
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker datePicker, int year, int month2, int day) {

            month_x = month2 + 1;
            year_x = year;
            day_x = day;
            clickableDate.setText((month_x) + "-" + day_x + "-" + year_x);
            dueDate = String.valueOf(month_x) + "-" + String.valueOf(day_x) + "-" + String.valueOf(year_x);
            addBillVewModel.setDueDate(dueDate);
        }
    };

    private void findViews() {
//        month = (TextInputEditText) findViewById(R.id.inputMonth);
//        monthLayout = (TextInputLayout) findViewById(R.id.monthLayout);
        company = (TextInputEditText) findViewById(R.id.inputCompany);
        companyLayout = (TextInputLayout) findViewById(R.id.companyNameLayout);
        amount = (TextInputEditText) findViewById(R.id.inputAmount);
        amountLayout = (TextInputLayout) findViewById(R.id.amountLayout);
        submitButton = (Button) findViewById(R.id.addBillButton);

        clickableDate = (TextView) findViewById(R.id.clickableDate);
        cardView = (CardView) findViewById(R.id.cardView);

    }

    private void configureCompanyNameEditText() {
        company.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //todo limit chars
                addBillVewModel.setCompanyName(editable.toString());
                if (editable.toString().length() < 3) {
                    companyLayout.setError("Company must be longer");
                    addBillVewModel.setCompanyInputChecker(false);
                } else {
                    companyLayout.setErrorEnabled(false);
                    addBillVewModel.setCompanyInputChecker(true);
                }
            }
        });
    }

    private void configureAmountEditText() {
        amount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //todo string clean, convert to number format
                //todo limit chars
                addBillVewModel.setAmount(editable.toString());
                if (editable.toString().length() <= 2) {
                    amountLayout.setError("Enter an amount");
                    addBillVewModel.setAmountInputChecker(false);
                } else {
                    amountLayout.setErrorEnabled(false);
                    addBillVewModel.setAmountInputChecker(true);
                }
                }

        });

    }

    private void configureViewModel() {
        addBillVewModel = ViewModelProviders.of(this).get(AddBillViewModel.class);

        addBillVewModel.getBill().observe(this, new Observer<Bill>() {
            @Override
            public void onChanged(@Nullable Bill bill) {
                if (bill != null) {
                  //  month.setText(bill.getMonth());
                    company.setText(bill.getCompanyName());
                    amount.setText(bill.getAmount());
                    clickableDate.setText("Due Date: " + bill.getDueDate());

                }
            }

        });

    }

    private void configureAddBillListener() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //prolly doesnt need to be an observable since no rotation risk of losing
                if (addBillVewModel.getShouldEnableNext()) {
                    Bill bill = new Bill(amount.getText().toString(), company.getText().toString(), dueDate);
                    addBillVewModel.addBill(bill);
                    Intent i = new Intent(AddBillActivity.this, MainActivity.class);
                    startActivity(i);
                } else {
                    Toast.makeText(AddBillActivity.this, "Fill out all the inputs", Toast.LENGTH_SHORT).show();
                }

            }
        });
    }

}
