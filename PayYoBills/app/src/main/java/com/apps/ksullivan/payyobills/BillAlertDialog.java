//package com.apps.ksullivan.payyobills;
//
//import android.app.AlertDialog;
//import android.app.Dialog;
//import android.content.DialogInterface;
//import android.os.Bundle;
//import android.support.annotation.NonNull;
//import android.support.v4.app.DialogFragment;
//
///**
// * Created by ksullivan on 10/16/17.
// */
//
//public class BillAlertDialog extends DialogFragment {
//
//    //todo i want to make this more reusable, call a method that provides the message, pos and neg button text
//    //todo the calling class should implement what to do click!
//
//    @NonNull
//    @Override
//    public Dialog onCreateDialog(Bundle savedInstanceState) {
//        //custom view optional, by using getLayoutInflater
//        AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
//        b.setMessage("Do you want to delete this bill?")
//                .setPositiveButton("YES", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        //delete
//                    }
//                })
//                .setNegativeButton("NO", new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        //return
//                    }
//                });
//
//                return b.create();
//
//    }
//}
