package com.apps.ksullivan.payyobills.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.apps.ksullivan.payyobills.BillDaoImpl;
import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.BillDatabase;

import java.util.List;

/**
 * Created by ksullivan on 10/2/17.
 */


//todo trying out ways to get the context into the viewmodel....theres a prolem rendering it on the ain activity
public class BillViewModel extends AndroidViewModel {

    private MutableLiveData<String> month;
    private LiveData<List<Bill>> bills;
    private BillDatabase database;

    public BillViewModel(Application application) {
        super(application);
        database = BillDaoImpl.getBillService().createDataBase(application);
        //bills = database.billDao().getUnpaidBills();

    }

    public LiveData<List<Bill>> getBills() {

        if (bills == null) {
            bills = new MutableLiveData<>();
            bills = BillDaoImpl.getBillService().getUnpaidBills();
        }

        return bills;
    }

}
