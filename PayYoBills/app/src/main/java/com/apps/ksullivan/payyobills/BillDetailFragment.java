package com.apps.ksullivan.payyobills;

import android.app.Dialog;
import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.viewmodel.BillDetailViewModel;
import com.apps.ksullivan.payyobills.viewmodel.BillViewModel;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ksullivan on 10/7/17.
 */

public class BillDetailFragment extends Fragment {

    private static final String BILL_ID = "bill_id";
    private static final String BILL_ITEM= "bill";
    private TextView detailDueDate;
    private TextView detailCompany;
    private TextView detailAmount;
    private Button button;
    private Button backButton;
    private BillDetailViewModel viewModel;
    private Bill bill;

    private CheckBox paidCheckBox;
    private Button editButton;
    private TextView billTypeLabel;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        //get arguments passed in in ocreate
        super.onCreate(savedInstanceState);
        bill = (Bill) getArguments().getSerializable(BILL_ITEM);
        configureViewModel();

    }

    private void configureViewModel() {
        viewModel = ViewModelProviders.of(this).get(BillDetailViewModel.class);
        viewModel.setBill(bill);
        viewModel.getBill().observe(this, new Observer<Bill>() {
            @Override
            public void onChanged(@Nullable Bill bill) {
                //only changeable part
                //needed for rotation i think
                if (bill != null) {
                    //since we are sharing this class between the main acttivity and paid bill activity
                    //i wanna hide the checkboc for bils the are paid

                    paidCheckBox.setChecked(bill.isPaid());
                }
            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bill_detail_fragment, container, false);
        findAndSetViews(view);
        setDeleteButton();
        setBackButton();
        setPaidCheckbox();
        setEditButton();
        return view;

    }

    private void setEditButton() {
        if(bill.isPaid()) {
            editButton.setVisibility(View.INVISIBLE);
        }
        editButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //todo lock ui on these async calls
                //call after execute to make sure complete
                if(paidCheckBox.isChecked()) {
                    Date d = Calendar.getInstance().getTime();
                    SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
                    String conDate = dateFormat.format(d);
                    bill.setPaidDate(conDate);
                }
                viewModel.addBill(bill);
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }
        });
    }

    private void findAndSetViews(View view) {
        detailDueDate = (TextView) view.findViewById(R.id.detailDueDate);
        detailCompany = (TextView) view.findViewById(R.id.detailCopmany);
        detailAmount = (TextView) view.findViewById(R.id.detailAmount);
        button = (Button) view.findViewById(R.id.deleteButton);
        backButton = (Button) view.findViewById(R.id.backButton);
        paidCheckBox = (CheckBox) view.findViewById(R.id.checkBox);
        editButton = (Button) view.findViewById(R.id.editButton);
        billTypeLabel = (TextView) view.findViewById(R.id.billTypeLabel);


        //should never be
        if (bill != null) {
            detailAmount.setText(bill.getAmount());
            detailCompany.setText(bill.getCompanyName());
            paidCheckBox.setChecked(bill.isPaid());
            if (bill.isPaid()) {
                billTypeLabel.setText("Paid Date");
                detailDueDate.setText(bill.getPaidDate());
            } else {
                detailDueDate.setText(bill.getDueDate());
                billTypeLabel.setText("Due Date");
            }

        }
    }

    private void setBackButton() {
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(), MainActivity.class);
                startActivity(i);
            }
        });
    }

    private void setDeleteButton() {

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //downside, if i implemented dialogfragment i would get onsaved instance for free, not here
                //upside this is more reusable
                DialogAlertModule.createAlertDialog(getActivity(), "Title", "Sure you wanna delete?", true, "YES", "NO", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getActivity(), "Deleted", Toast.LENGTH_SHORT).show();
                                viewModel.deleteBill(bill);
                                Intent intent = new Intent(getActivity(), MainActivity.class);
                                startActivity(intent);
                            }
                        }, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                Toast.makeText(getActivity(), "Dismissed", Toast.LENGTH_SHORT).show();
                            }
                        }).show();
            }
        });
    }

    public static BillDetailFragment newInstance(Bill bill) {
        //if i converted this to an id....i could store it in the notigication and use it to launcg a detail view

        //BillPagerActivity sends us the bill of interest so we know which one to inflate
        Bundle args = new Bundle();
        args.putSerializable(BILL_ITEM, bill);
        BillDetailFragment fragment = new BillDetailFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public void setPaidCheckbox() {
        //todo wrap this up in the bill object
        //todo create an update method
        //todo write logic for if boolean is paid set the checked button to trueeeeee
        //todo change the pink background when clicked to blue
        if(bill.isPaid()) {
            paidCheckBox.setVisibility(View.INVISIBLE);
        }
        paidCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (compoundButton.isChecked()) {
                    paidCheckBox.setChecked(true);
                    //setting the local bill
                    bill.setPaid(true);
                } else if (!compoundButton.isChecked()) {
                    paidCheckBox.setChecked(false);
                    bill.setPaid(false);
                }
            }
        });

    }

}
