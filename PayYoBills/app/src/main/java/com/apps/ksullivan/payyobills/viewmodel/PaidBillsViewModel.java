package com.apps.ksullivan.payyobills.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.apps.ksullivan.payyobills.BillDaoImpl;
import com.apps.ksullivan.payyobills.db.Bill;

import java.util.List;

/**
 * Created by ksullivan on 11/6/17.
 */

public class PaidBillsViewModel extends AndroidViewModel {
    public PaidBillsViewModel(Application application) {
        super(application);
        //do i need to call the create db?
    }

    LiveData<List<Bill>> paidBills;

//    public LiveData<List<Bill>> getPaidBills() {
//        List<Bill> pBills = new ArrayList<>();
//    //stream woulndt work as toList needs api 24 and up, this is 23
//        if (paidBills == null) {
//            paidBills = new MutableLiveData<>();
//            List<Bill> allBills = BillServiceImpl.getBillService().getPaidBills().getValue();
//            if (allBills != null) {
//                for (Bill b : allBills) {
//                    if (b.isPaid()) {
//                        pBills.add(b);
//                    }
//                }
//                paidBills.setValue(pBills);
//            }
//
//        }
//        return paidBills;
//    }

    public LiveData<List<Bill>> getPaidBills() {
        if (paidBills == null) {
            paidBills = new MutableLiveData<>();
            paidBills = BillDaoImpl.getBillService().getPaidBills();
        }

        return paidBills;
    }

}
