package com.apps.ksullivan.payyobills.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.os.AsyncTask;

import com.apps.ksullivan.payyobills.BillDaoImpl;
import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.BillDatabase;

/**
 * Created by ksullivan on 10/8/17.
 */

public class BillDetailViewModel extends AndroidViewModel {

    private MutableLiveData<Bill> bill;
    private BillDatabase database;


    public BillDetailViewModel(Application application) {
        super(application);
            database = BillDaoImpl.getBillService().createDataBase(application);
            //bills = database.billDao().getUnpaidBills();

    }

    public MutableLiveData<Bill> getBill() {
        if (bill == null) {
            bill = new MutableLiveData<>();
            bill.setValue(null);
        }
         return bill;
    }

    public void setBill(Bill billInput) {
        if (bill == null) {
            bill = new MutableLiveData<>();
        }

        this.bill.setValue(billInput);
    }

    public void deleteBill(Bill bill) {
        DeleteBillTask deleteBillTask = new DeleteBillTask();
        deleteBillTask.execute(bill);
    }

    private class DeleteBillTask extends AsyncTask<Bill, Void, Void> {

        @Override
        protected Void doInBackground(Bill... item) {
            BillDaoImpl.getBillService()
                    .deleteBill(item[0]);
            return null;
        }
    }

    //should be able to just add the bill here
    //todo need another button to wire this up
    public void addBill(Bill bill){
        new AddBillTask().execute(bill);
    }

    private class AddBillTask extends AsyncTask<Bill, Void, Void> {

        @Override
        protected Void doInBackground(Bill... item) {
            BillDaoImpl.getBillService().addBill((item[0]));
            return null;
        }
    }

}
