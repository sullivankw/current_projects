package com.apps.ksullivan.payyobills;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.viewmodel.BillPagerViewModel;

import java.util.List;

/**
 * Created by ksullivan on 10/13/17.
 */

public class BillPagerActivity extends AppCompatActivity{

    //todo pass in an id that tells which activity called it
    public static final String BILL_ID = "bill_id";
    private ViewPager viewPager;
    private BillPagerViewModel billPagerViewModel;
    private int billID;
    private List billList;
    private BillPagerViewAdaptor pagerAdaptor;
    private String activityID;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bill_pager);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("BILL DETAIL");
        setSupportActionBar(toolbar);
        getExtras();
        configureViewPagerAdaptor();
        configureViewModel();
    }

    private void configureViewPagerAdaptor() {
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        FragmentManager manager = getSupportFragmentManager();
        pagerAdaptor = new BillPagerViewAdaptor(manager);
        viewPager.setAdapter(pagerAdaptor);

    }

    private void getExtras(){
        billID = (int) getIntent().getIntExtra(BILL_ID, 1);
        activityID = (String) getIntent().getSerializableExtra("activity_id");

    }

    private void configureViewModel() {
        billPagerViewModel = ViewModelProviders.of(this).get(BillPagerViewModel.class);
        billPagerViewModel.setCallingActivity(activityID);
        billPagerViewModel.getBills().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                if (bills != null) {
                    billList = bills;
                    pagerAdaptor.notifyDataSetChanged();
                    for (int i = 0; i < billList.size(); i++) {
                        Bill b = (Bill) billList.get(i);
                        if (b.getId() == billID) {
                            viewPager.setCurrentItem(i);
                            break;
                        }
                    }
                }
            }
        });
    }

    class BillPagerViewAdaptor extends FragmentStatePagerAdapter {

        public BillPagerViewAdaptor(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            Bill bill = (Bill) billList.get(position);
            return BillDetailFragment.newInstance(bill);
        }

        @Override
        public int getCount() {
                if (billList != null) {
                    return billList.size();
                } else
                    return 0;
            }

        @Override
        public CharSequence getPageTitle(int position) {
            return super.getPageTitle(position);
        }

    }
}