package com.apps.ksullivan.payyobills.db;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ksullivan on 11/13/17.
 */

public class GsonBills implements Serializable {

    private List<Bill> bills;

    public List<Bill> getBills() {
        return bills;
    }

    public void setBills(List<Bill> bills) {
        this.bills = bills;
    }
}
