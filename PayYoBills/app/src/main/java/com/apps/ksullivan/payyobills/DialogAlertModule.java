package com.apps.ksullivan.payyobills;

import android.content.Context;
import android.content.DialogInterface;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;

/**
 * Created by ksullivan on 10/16/17.
 */

public class DialogAlertModule {

    public static AlertDialog createAlertDialog(Context context, String title, String message, boolean cancellable, @NonNull String positiveButtonText,
                                                @Nullable String negButtonText, @Nullable DialogInterface.OnClickListener posListener,
                                                @Nullable DialogInterface.OnClickListener negListener) {
        return new AlertDialog.Builder(context)
                .setTitle(title)
                .setCancelable(cancellable)
                .setMessage(message)
                .setPositiveButton(positiveButtonText, posListener)
                .setNegativeButton(negButtonText, negListener)
                .create();
        }
}
