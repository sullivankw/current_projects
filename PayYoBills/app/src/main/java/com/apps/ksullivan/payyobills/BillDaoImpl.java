package com.apps.ksullivan.payyobills;

import android.arch.lifecycle.LiveData;
import android.content.Context;
import android.provider.Settings;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.db.BillDatabase;

import java.util.List;

/**
 * Created by ksullivan on 10/2/17.
 * Will be used as a mock source of data to get the project going
 * Will connect to a Room DB in the future
 */

public class BillDaoImpl implements BillDao {

    private BillDatabase billDatabase;
    private LiveData<List<Bill>> bills;
    private LiveData<List<Bill>> paidBills;
    private static BillDaoImpl INSTANCE;

    public static BillDaoImpl getBillService() {
        if (INSTANCE == null) {
            INSTANCE = new BillDaoImpl();
        }
        //createDataBase()

        return INSTANCE;
    }

    public BillDatabase createDataBase(Context c) {
        billDatabase = BillDatabase.getDatabase(c);
        //do i need the call to get bills here?
        //bills = billDatabase.billDao().getUnpaidBills();
        return billDatabase;
    }

    public LiveData<List<Bill>> getUnpaidBills() {
        bills = billDatabase.billDao().getBills(false);
        return bills;
    }

//    @Override
//    public LiveData<List<Bill>> getPaidBills() {
//        paidBills = billDatabase.billDao().getUnpaidBills();
//        List<Bill> c = paidBills.getValue();
//
//        return bills;
//    }

    public LiveData<List<Bill>> getPaidBills() {
        bills = billDatabase.billDao().getPaidBills(true);
        return bills;
    }

    public LiveData<Bill> getBill(int id) {
        return billDatabase.billDao().getBill(id);

    }

    public void addBill(Bill bill) {
        billDatabase.billDao().addBills(bill);
    }

    public void deleteBill(Bill bill) {
        billDatabase.billDao().deleteBill(bill);
    }

}
