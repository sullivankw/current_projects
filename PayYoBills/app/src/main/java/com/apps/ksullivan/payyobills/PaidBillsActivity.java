package com.apps.ksullivan.payyobills;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.apps.ksullivan.payyobills.db.Bill;
import com.apps.ksullivan.payyobills.viewmodel.BillViewModel;
import com.apps.ksullivan.payyobills.viewmodel.PaidBillsViewModel;

import java.util.List;

public class PaidBillsActivity extends BaseBillActivity implements BillRecyclerViewAdaptor.BillOnClickListener  {

    private PaidBillsViewModel paidBillsViewModel;
    private List<Bill> paidBills;
    private BillRecyclerViewAdaptor recyclerViewAdaptor;
    private RecyclerView recyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paid_bills);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle("BILLS PAID");
        setSupportActionBar(toolbar);
        recyclerView = (RecyclerView) findViewById(R.id.recyclerViewPaid);

        configureRecyclerView();
        configureViewModel();


    }

    private void configureRecyclerView() {
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, 1);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerViewAdaptor = new BillRecyclerViewAdaptor(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(recyclerViewAdaptor);



    }

    private void configureViewModel() {
        paidBillsViewModel = ViewModelProviders.of(this).get(PaidBillsViewModel.class);
        paidBillsViewModel.getPaidBills().observe(this, new Observer<List<Bill>>() {
            @Override
            public void onChanged(@Nullable List<Bill> bills) {
                if (bills != null) {
                    recyclerViewAdaptor.setBillsList(bills);
                    //notifiy change?
                }
            }

        });
    }

    @Override
    public void onBillClicked(Bill bill) {

        Intent i = new Intent(this, BillPagerActivity.class);
        i.putExtra("bill_id", bill.getId());
        i.putExtra("activity_id", "PaidBillsActivity");
        startActivity(i);
    }
}
