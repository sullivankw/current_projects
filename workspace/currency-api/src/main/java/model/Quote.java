package model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Quote {
	
	private Double USDAED;
	private Double USDAFN;
	private Double USDALL;
	public Double getUSDAED() {
		return USDAED;
	}
	public void setUSDAED(Double uSDAED) {
		USDAED = uSDAED;
	}
	public Double getUSDAFN() {
		return USDAFN;
	}
	public void setUSDAFN(Double uSDAFN) {
		USDAFN = uSDAFN;
	}
	public Double getUSDALL() {
		return USDALL;
	}
	public void setUSDALL(Double uSDALL) {
		USDALL = uSDALL;
	}

}
