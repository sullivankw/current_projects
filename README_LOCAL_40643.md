# README #

### What is this repository for? ###

* Current projects by Kevin Sullivan
* I am a professional software developer specializing in Java web services. As Android development is an interest of mine most of my freetime 
* projects are written using the Android framework. 

### How do I get set up? ###

* The blackjack app can be found on the Google Play Store:
* https://play.google.com/store/apps/details?id=com.blackjackadvice.ksullivan.blackjackadvice&hl=en


* CurrencyApiTest is just a simple Spring Boot App that makes a call
* using RestTemplate to a currency conversion web service I am 
* looking to leverage in an Android App

* MortgageSoapProject is simply a soap client the calls a service to get up to date mortgage rate information


* BillTracker is an app in the mergopopopotest stages. 

