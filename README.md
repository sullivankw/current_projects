# README #

### What is this repository for? ###

* Current projects by Kevin Sullivan
* I am a professional software developer specializing in Java web services.

### How do I get set up? ###

* The blackjack app can be found on the Google Play Store:
* https://play.google.com/store/apps/details?id=com.blackjackadvice.ksullivan.blackjackadvice&hl=en


* CurrencyApiTest is just a simple Spring Boot App that makes a call
* using RestTemplate to a currency conversion web service I am 
* looking to leverage in an Android App

* MortgageSoapProject is simply a soap client the calls a service to get up to date mortgage rate information



### Current Todos for PayYoBills App ###
* Clean up JobScheduler 
* If possible, can I pass the bill id to the notification and return it as an extra...use that to launch from click to the 
* corresponding detail fragment. I would have to chnage the param from an entire Bill to expect and id and iterate and find the bill in that method
* Assure timed job is as scheduled
* Prettify all card views
* Fix wacky color, including inconsistent text
* Add proper validation on and bill input fields....including date picker

### Current Todos for WeatherApp ###
* Add field validation for AddCityActivity
* Custom error handling for retrofit client
* Need lotsa null checks

![Detail View](WeatherAppRecViewWithDetailView.png)

![Add City](WeatherAppAddCity.png)

### PhotoApp ###
* App fetches photos based on serach tag from flikr

![Search View](PhotoAppSearchView.png)

![Filter View](PhotoAppNoConnection.png)

![Connection Error View](PhotoAppFilterView.png)

