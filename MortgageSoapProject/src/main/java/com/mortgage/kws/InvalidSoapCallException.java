package com.mortgage.kws;

public class InvalidSoapCallException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1157447335522161027L;
	
	private String errorCode;
	//Todo create custom handler for soap faults
	
	public InvalidSoapCallException (String reason, String errorCode) {
		super(reason);
		this.errorCode = errorCode;
		
	}

}
