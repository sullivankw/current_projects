package com.mortgage.kws;

import javax.xml.soap.Name;
import javax.xml.ws.soap.SOAPFaultException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapFault;
import org.springframework.ws.soap.SoapFaultException;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.mortgage.kws.model.GetCurrentMortgageIndexByWeekly;
import com.mortgage.kws.model.GetCurrentMortgageIndexByWeeklyResponse;
import com.mortgage.kws.model.GetCurrentMortgageIndexMonthlyResponse;
import com.mortgage.kws.model.GetMortgageIndexByMonth;
import com.mortgage.kws.model.GetMortgageIndexByMonthResponse;

@Component
public class MortgageClient extends WebServiceGatewaySupport {
	
	@Value("${service.soap.url}")
	private String url;

	@Value("${service.soap.callback}")
	 private String soapCallback;
		
	@Value("${service.soap.monthly.callback}")
	 private String soapCallbackMonthly;
	
	private static final Logger log = LoggerFactory.getLogger(MortgageClient .class);
	
	public GetCurrentMortgageIndexByWeeklyResponse getCurrentMortgageIndexByWeekly() throws Exception {
	
		
		GetCurrentMortgageIndexByWeekly request = new GetCurrentMortgageIndexByWeekly();
		GetCurrentMortgageIndexByWeeklyResponse response = null;
		try {
		response = (GetCurrentMortgageIndexByWeeklyResponse) getWebServiceTemplate()
				.marshalSendAndReceive(url,
						request,
						new SoapActionCallback(soapCallback));
		} catch(Exception c) {
			if (c instanceof InvalidSoapCallException) {
				//TODO configure debugger
				log.debug("soap request could not be completed. The mortgage api does not provide a modeled fault exception");
				throw new InvalidSoapCallException(c.getMessage(),"Error connecting with the Mortgage Service");			
			} else {
				throw new Exception("Problem happened");
			}
					
		}
		
		log.info("Call success at " + response.getGetCurrentMortgageIndexByWeeklyResult().getIndexDate().toString());
		
        return response;
    }
	
	public GetMortgageIndexByMonthResponse getCurrentMortgageIndexByMonth() {
		
		GetMortgageIndexByMonthResponse response = null;
		GetMortgageIndexByMonth request = new GetMortgageIndexByMonth();
		request.setMonth(12);
		request.setYear(2014);
		try {
			response = (GetMortgageIndexByMonthResponse) getWebServiceTemplate()
					.marshalSendAndReceive(url,
							request,
							new SoapActionCallback(soapCallbackMonthly));
			
		} catch(Exception e) {
			
		}
		
		
		return response;
	}

}
