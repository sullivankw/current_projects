package com.mortgage.kws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.mortgage.kws.model.GetCurrentMortgageIndexByWeeklyResponse;
import com.mortgage.kws.model.GetMortgageIndexByMonth;
import com.mortgage.kws.model.GetMortgageIndexByMonthResponse;

@Component
public class MortgageService implements CommandLineRunner{
	
	
	@Autowired
	MortgageClient client;
	
	public void getMortgageRatesWeekly() throws Exception {
		
		GetCurrentMortgageIndexByWeeklyResponse response = client.getCurrentMortgageIndexByWeekly();
		String treasuryBilly = response.getGetCurrentMortgageIndexByWeeklyResult().getSixMonthTreasuryBill();
		System.out.println("Current treasury bill");
		System.out.println(treasuryBilly);
		System.out.println("---------------------");
		System.out.println("6 month cd: " + response.getGetCurrentMortgageIndexByWeeklyResult().getThreeMonthSecondaryMarketCD());
		
		
	}
	
	public void getMortgageRatesByMonth() {
		GetMortgageIndexByMonthResponse response = client.getCurrentMortgageIndexByMonth();
		System.out.println("This reponse is coming back with no results but the call is good");
		System.out.println("Deposit index cost: " + response.getGetMortgageIndexByMonthResult().getCostOfDepositsIndex());
				
		
	}
	
	@Override
	public void run(String... arg0) throws Exception {
		this.getMortgageRatesWeekly();
		this.getMortgageRatesByMonth();
		
	}

}
