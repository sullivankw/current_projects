package com.mortgage.kws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;

import com.mortgage.kws.model.GetCurrentMortgageIndexByWeekly;
import com.mortgage.kws.model.GetCurrentMortgageIndexByWeeklyResponse;

@Configuration
public class SoapClientConfig {
		  	 
		@Value("${service.soap.url}")
		private String url;
	
	    @Bean
	    public Jaxb2Marshaller marshaller() {
	        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	        marshaller.setContextPath("com.mortgage.kws.model");
	        return marshaller;
	    }

	    @Bean
	    public MortgageClient mortgageClient(Jaxb2Marshaller marshaller) {
	    	MortgageClient client = new MortgageClient();
	        client.setDefaultUri(url);
	        client.setMarshaller(marshaller);
	        client.setUnmarshaller(marshaller);
	        return client;
	    }
	    
	}
