package com.mortgage.kws;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;

import com.mortgage.kws.model.GetCurrentMortgageIndexByWeeklyResponse;

@SpringBootApplication
public class MortgageSoapProjectApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(MortgageSoapProjectApplication.class, args);
	
	}
		
}
