package com.blackjackadvice.ksullivan.blackjackadvice.versionLogic;

/**
 * Created by ksullivan on 2/3/17.
 */

public class AdviceCalculatorService {
    private HitSoft17 hitSoft17 = new HitSoft17();
    private StaySoft17 staySoft17 = new StaySoft17();

    public String getHandAdvice(String dealerCard, String playerCardA, String playerCardB, Boolean switchHitSoft17) {

        if (!switchHitSoft17) {

            return staySoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);

        } else
        //if switch is true this path is used
        return hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
    }


    //if this was decoupled i could extract this layer out, convert this to java beans, and return the
    //it as a webservice back to my android app.


}
