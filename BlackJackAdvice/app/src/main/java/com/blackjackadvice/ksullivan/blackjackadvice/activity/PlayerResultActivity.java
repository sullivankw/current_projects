package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.DialogFragment;
import android.app.LoaderManager;
import android.content.ContentValues;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.DialogListener;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.adaptor.ResultsCursorAdaptor;
import com.blackjackadvice.ksullivan.blackjackadvice.data.DatabaseHelper;
import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract.ResultsEntry;
import com.blackjackadvice.ksullivan.blackjackadvice.data.ResultsQueryHandler;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.InsertAmountDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.InsertResultDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.model.Result;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ksullivan on 2/6/17.
 */

public class PlayerResultActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, DialogListener,
        InsertResultDialogFragment.InsertResultDialogFragmentListener, InsertAmountDialogFragment.InsertAmountDialogFragmentListener {

    static final int ALL_RECORDS = -1;
    private static final int URL_LOADER = 0;
    public static final String GLOBAL_MONTH = "globalMonth";
    public static final String GLOBAL_YEAR = "globalYear";
    public static final String GLOBAL_DAY = "globalDay";
    private Cursor cursor;
    private EditText editViewA;
    private EditText editViewB;
    private TextView textView;
    private ResultsCursorAdaptor adaptor;
    private Result result;
    private ResultsQueryHandler handler;
    private String myBuyIn;
    private String myCashOut;
    private boolean goNow;
    private boolean deleteAll = false;

//***************SearchYearStuff
    private int searchYear;
    private int searchDay;
    private int searchMonth;
    private String dateFromSearchAct;

    private boolean viewAll = false;



    public String buyInListener;
    public String cashOutListener;


    //Date stuff
    private Button dateBtn;
    private Button addResultBtn;
    public int year_x,month_x,day_x;
    static final int DILOG_ID = 0;

    //GettingValues fro search
    private String profitValueReturnedFromSearchAct;
    private boolean orderResults;

    private int monthGlobal, yearGlobal, dayGlobal;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_result_activity);
        getLoaderManager().initLoader(URL_LOADER, null, this);

        final ListView lv = (ListView) findViewById(R.id.showAllResults);
        addResultBtn = (Button) findViewById(R.id.addResultBtn);

        getSearchResultsFromSearchActivity(savedInstanceState);

        if (savedInstanceState != null) {
        //todo get all variables, getSearchResults Method takes care of it's saved instances
            monthGlobal = savedInstanceState.getInt(GLOBAL_MONTH);
            yearGlobal = savedInstanceState.getInt(GLOBAL_YEAR);
            dayGlobal = savedInstanceState.getInt(GLOBAL_DAY);
        }


        //******CAL STUFF
        //todo am i still using this?
        final Calendar cal = Calendar.getInstance();
        year_x = cal.get(Calendar.YEAR);
        month_x = cal.get(Calendar.MONTH);
        day_x = cal.get(Calendar.DAY_OF_MONTH);


        adaptor = new ResultsCursorAdaptor(this, cursor, false);
        lv.setAdapter(adaptor);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//            toolbar.setElevation(10f);
//            getSupportActionBar().setTitle("BlackJack Advice");
//        }


        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                cursor = (Cursor) parent.getItemAtPosition(position);

                int resultId = cursor.getInt(cursor.getColumnIndex(ResultsEntry._ID));
                String date = cursor.getString(cursor.getColumnIndex(ResultsEntry.COLUMN_DATE));
                String startBalance = cursor.getString(cursor.getColumnIndex(ResultsEntry.COLUMN_START_BALANCE));
                String endBalance = cursor.getString(cursor.getColumnIndex(ResultsEntry.COLUMN_END_BALANCE));
                String profit = cursor.getString(cursor.getColumnIndex(ResultsEntry.COLUMN_PROFIT));
                Result r = new Result(resultId, date, startBalance, endBalance, profit);
                Intent intent = new Intent(PlayerResultActivity.this, SinglePlayerResultActivity.class);
                intent.putExtra(Constants.RESULT_OBJECT, r);
                startActivity(intent);
            }
        });

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        outState.putString(Constants.SEARCH_PROFITS, profitValueReturnedFromSearchAct);
        outState.putBoolean(Constants.SEARCH_SORT_ORDER, orderResults);
        outState.putInt(Constants.SEARCH_DAY, searchDay);
        outState.putInt(Constants.SEARCH_MONTH, searchMonth);
        outState.putInt(Constants.SEARCH_YEAR, searchYear);
        outState.putInt(GLOBAL_MONTH, monthGlobal);
        outState.putInt(GLOBAL_YEAR, yearGlobal);
        outState.putInt(GLOBAL_DAY, dayGlobal);

    }

    private void getSearchResultsFromSearchActivity(@Nullable Bundle savedInstanceState) {

        if (savedInstanceState == null) {
            Bundle extras = getIntent().getExtras();
            if (extras == null) {
                profitValueReturnedFromSearchAct = null;
                orderResults = false;
            } else {
                profitValueReturnedFromSearchAct = extras.getString(Constants.SEARCH_PROFITS);
                orderResults = extras.getBoolean(Constants.SEARCH_SORT_ORDER);
                searchDay = extras.getInt(Constants.SEARCH_DAY);
                searchMonth = extras.getInt(Constants.SEARCH_MONTH);
                searchYear = extras.getInt(Constants.SEARCH_YEAR);

            }
        } else if (savedInstanceState !=null) {
            profitValueReturnedFromSearchAct = savedInstanceState.getString(Constants.SEARCH_PROFITS);
            orderResults = savedInstanceState.getBoolean(Constants.SEARCH_SORT_ORDER);
            searchDay = savedInstanceState.getInt(Constants.SEARCH_DAY);
            searchMonth = savedInstanceState.getInt(Constants.SEARCH_MONTH);
            searchYear = savedInstanceState.getInt(Constants.SEARCH_YEAR);

        }

        if (searchDay > 0) {

            String stringYear = String.valueOf(searchYear);
            String stringMonth = String.valueOf(searchMonth + 1);
            StringBuilder sb = new StringBuilder();
            if (stringMonth.length() == 1) {
                sb.append(stringMonth).append("0");
                sb.reverse();
                stringMonth = sb.toString();
            }
            sb = new StringBuilder();
            String stringDay = String.valueOf(searchDay);
            if (stringDay.length() == 1) {
                sb.append(stringDay).append("0");
                sb.reverse();
                stringDay = sb.toString();
            }

            dateFromSearchAct = stringYear + "-" + stringMonth + "-" + stringDay;

        }


    }

    @Override
    protected void onResume() {
        //todo this method makes the loader run 2x but when i take it out the list doesnt render
        //todo how to fix?
        getLoaderManager().restartLoader(URL_LOADER, null ,this);
        super.onResume();

    }

    public void createIndividualResultEntryOG(String buyInListener, String cashOutListener, int month, int year, int day) {

//CHECKS THAT NO SEARCH CRITERIA IS ALREADY LOADED WHEN A NEW ENTRY IS ENTERED....THIS WAY IT RELOADS THE ENTIRE LIST AFTER EACH ENTRY IS ENTERED REGARDLESS
        if (profitValueReturnedFromSearchAct != null ||
                dateFromSearchAct != null) {
            setViewAllFlag();
            onResume();
            resetVariables();
        }

        int buyIn = Integer.parseInt(buyInListener);
        int cashOut = Integer.parseInt(cashOutListener);

        String stringYear = String.valueOf(year);
        String stringMonth = String.valueOf(month + 1);
        StringBuilder sb = new StringBuilder();
        if (stringMonth.length() == 1) {
            sb.append(stringMonth).append("0");
            sb.reverse();
            stringMonth = sb.toString();
        }
        sb = new StringBuilder();
        String stringDay = String.valueOf(day);
        if (stringDay.length() == 1) {
            sb.append(stringDay).append("0");
            sb.reverse();
            stringDay = sb.toString();
        }

        String stringDatePickerDate = stringYear + "-" + stringMonth + "-" + stringDay;


        ContentValues values = new ContentValues();

        values.put(ResultsEntry.COLUMN_DATE, stringDatePickerDate);
        values.put(ResultsEntry.COLUMN_START_BALANCE, buyIn );
        values.put(ResultsEntry.COLUMN_END_BALANCE, cashOut);
        values.put(ResultsEntry.COLUMN_PROFIT, cashOut - buyIn );


        ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
        handler.startInsert(1, null, ResultsEntry.CONTENT_URI, values);

        Toast.makeText(this, "Live blackjack session added.", Toast.LENGTH_SHORT).show();

    }

    public void launchInsertResultDialogFragment(View view) {
        InsertResultDialogFragment insertResultDialogFragment = new InsertResultDialogFragment();
        insertResultDialogFragment.show(getFragmentManager(), "insertDialog");

    }

    //for enteringDates
    @Override
    public void onDialogPositiveClick(DialogFragment dialog, final int month, final int year, final int day) {

        AlertDialog.Builder builder = new
                AlertDialog.Builder(PlayerResultActivity.this);
        builder.setMessage(getString(R.string.insert_entry_confirm)).setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        getMonth(month);
                        getYear(year);
                        getDay(day);

                        InsertAmountDialogFragment s = new InsertAmountDialogFragment();
                        s.show(getFragmentManager(), "insertDialog");

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
        AlertDialog alert = builder.create();
        //alert.setTitle(R.string.delete_single_result_title);
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.show();


    }

    private int getMonth(int month) {
        return this.monthGlobal = month;
    }

    private int getYear(int year) {
        return this.yearGlobal = year;
    }

    private int getDay(int day) {
        return this.dayGlobal = day;
    }

    //********SECOND DIALOG LISTENER, IT TAKES IN THE BUY IN AND CASH OUT AMOUNTS
    @Override
    public void onDialogPositiveClick(DialogFragment fragment, String buyInListener, String cashOutListener) {

        this.buyInListener = buyInListener;
        this.cashOutListener = cashOutListener;

        createIndividualResultEntryOG(buyInListener, cashOutListener, this.monthGlobal, this.yearGlobal, this.dayGlobal);
    }

    @Override
    public void onDialogNegativeClick(DialogFragment fragment) {
        Toast.makeText(this, "Record not inserted", Toast.LENGTH_SHORT).show();

    }

    //using uri version
    public void readDataAll() {
        DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();


        String [] projection = {ResultsEntry.COLUMN_DATE, ResultsEntry.COLUMN_START_BALANCE,
                ResultsEntry.COLUMN_END_BALANCE, ResultsEntry.COLUMN_PROFIT};
        int date1 = 10;
        int date2 = 20;
        //selection
        String selection = ResultsEntry._ID + " = ?";
        //SQLiteDatabase db = getReadableDatabase();
        //selection args only accepts strings, sql takes care of the  conversions
        //of course this will be the input from the user eventually

        //args is the value that is substitued for the question mark in the selection filter
        //in this version wwe are returning all records
       // Cursor c  = getContentResolver().query(ResultsEntry.CONTENT_URI, projection, null, null, null);
        Cursor c  = db.rawQuery("select * from results" +
                " where profit between " + date1  + " and " + date2 , null);
        int i = c.getCount();
        String rowContent = " ";
        while (c.moveToNext()) {
            for (int b=0; b<i; b ++) {
                rowContent += c.getString(b) + " - ";
            }
        }
        c.close();

    }

    public void readDataAllListView() {


        String [] projection = {ResultsEntry.COLUMN_DATE,
                ResultsEntry.TABLE_NAME + "." + ResultsEntry._ID,
                ResultsEntry.COLUMN_START_BALANCE,
                ResultsEntry.COLUMN_END_BALANCE,
                ResultsEntry.COLUMN_PROFIT};
        String value1 ="20";
        String value2 ="30";

        String[] args = {value1, value2};
        //ResultsEntry.COLUMN_PROFIT + " =?";

        //cursor  = getContentResolver().query(ResultsEntry.CONTENT_URI, projection, null, null, null);
        cursor  = getContentResolver().query(ResultsEntry.CONTENT_URI, projection,
                ResultsEntry.COLUMN_PROFIT + " =? AND " + ResultsEntry.COLUMN_END_BALANCE + " =?", args, null);

        //just cant get it to compile
        //cursor  = getContentResolver().query(ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_PROFIT + " BETWEEN =? AND =?", args, null);

        int i = cursor.getCount();

        String rowContent = " ";
        while (cursor.moveToNext()) {
            for (int b=0; b<i; b ++) {
                rowContent += cursor.getString(b) + " - ";
            }

        }

    }

    public void launchDeleteDataDialog(int id) {

        AlertDialog.Builder builder = new
                AlertDialog.Builder(PlayerResultActivity.this);
        builder.setMessage(getString(R.string.delete_all_results_dialog)).setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteAllResults();
                        Toast.makeText(PlayerResultActivity.this, "All entries deleted!", Toast.LENGTH_SHORT).show();

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        //deleteBox.setChecked(false);

                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle(R.string.delete_single_result_title);
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.show();

    }

    public void deleteAllResults() {
        String[] args = null;

        ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
        handler.startDelete(1, null, ResultsEntry.CONTENT_URI, ResultsEntry._ID + " =?", args);

    }

    public void updateData() {
        int id = 5;
        DatabaseHelper helper = new DatabaseHelper(this);
        SQLiteDatabase db = helper.getReadableDatabase();
        String[] args = {String.valueOf(id)};
        ContentValues values = new ContentValues();
        values.put(ResultsEntry.COLUMN_END_BALANCE, 200);
        //note resultsEntry._ID serves as the where clause
        int numRows = db.update(ResultsEntry.TABLE_NAME, values, ResultsEntry._ID + " =?", args);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_player_result_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent intent;
        if (id == R.id.action_go_to_main_activity) {
            intent = new Intent(PlayerResultActivity.this, MainActivity.class );
            startActivity(intent);
        } if (id == R.id.action_delete_all_results) {
            launchDeleteDataDialog(ALL_RECORDS);
        } if (id == R.id.action_go_search) {
            intent = new Intent(PlayerResultActivity.this, SearchActivity.class );
            startActivity(intent);
        } if (id == R.id.action_show_all) {
            setViewAllFlag();
            onResume();
            resetVariables();
        }

        return super.onOptionsItemSelected(item);
    }

    private void resetVariables() {
        profitValueReturnedFromSearchAct = null;
        dateFromSearchAct = null;
        searchDay = 0;
        searchMonth = 0;
        searchYear = 0;
        orderResults = false;
        monthGlobal = 0;
        yearGlobal = 0;
        dayGlobal = 0;
    }

    private void setViewAllFlag() {
        this.viewAll = true;
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {

        String [] projection = {ResultsEntry.COLUMN_DATE,
                ResultsEntry.TABLE_NAME + "." + ResultsEntry._ID,
                ResultsEntry.COLUMN_START_BALANCE,
                ResultsEntry.COLUMN_END_BALANCE,
                ResultsEntry.COLUMN_PROFIT};

        String sortOrder;
        if (!orderResults) {
            sortOrder = ResultsEntry.COLUMN_PROFIT + " DESC";
        } else {
            sortOrder = ResultsEntry.COLUMN_PROFIT + " ASC";
        }

        String dateOrder = ResultsEntry.COLUMN_DATE + " DESC";

        int queryCondition = getSearchQueryCriteria();

        switch(queryCondition) {
            case 1:
                String[] searchArgs = new String[1];
                searchArgs[0] = profitValueReturnedFromSearchAct;
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_PROFIT + " >=? ", searchArgs, sortOrder);
            case 2:
                searchArgs = new String[2];
                searchArgs[0] = profitValueReturnedFromSearchAct;
                searchArgs[1] = dateFromSearchAct;
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_PROFIT + " >=? AND " + ResultsEntry.COLUMN_DATE + " =?", searchArgs, sortOrder);
            case 3:
                searchArgs = new String[1];
                searchArgs[0] = profitValueReturnedFromSearchAct;
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_PROFIT + " <=? ", searchArgs, sortOrder);
            case 4:
                searchArgs = new String[2];
                searchArgs[0] = profitValueReturnedFromSearchAct;
                searchArgs[1] = dateFromSearchAct;
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_PROFIT + " <=? AND " + ResultsEntry.COLUMN_DATE + " =?", searchArgs, sortOrder);
            case 5:
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, null, null, dateOrder);
            case 6:
                searchArgs = new String[1];
                searchArgs[0] = dateFromSearchAct;
                return new CursorLoader(this, ResultsEntry.CONTENT_URI, projection, ResultsEntry.COLUMN_DATE + " =? ", searchArgs, dateOrder);
            default:
                return null;


        }

    }

    private int getSearchQueryCriteria() {
        //IF VIEW ALL IS SELECTED BY THE USER WE SET THE QUERY CUSTOMER TO 5 IN ORDER TO QUERY ALL RESULTS IN THE TABLE
        int queryCondition = 0;

        if (!viewAll) {

            if (profitValueReturnedFromSearchAct != null) {
                if (Integer.parseInt(profitValueReturnedFromSearchAct) > 0 && dateFromSearchAct == null) {
                    queryCondition = 1;
                } else if (Integer.parseInt(profitValueReturnedFromSearchAct) > 0 && dateFromSearchAct != null) {
                    queryCondition = 2;
                } else if (Integer.parseInt(profitValueReturnedFromSearchAct) < 0 && dateFromSearchAct == null) {
                    queryCondition = 3;
                } else if (Integer.parseInt(profitValueReturnedFromSearchAct) < 0 && dateFromSearchAct != null) {
                    queryCondition = 4;
                }
            } else {
                if (dateFromSearchAct == null) {
                    queryCondition = 5;
                } else {
                    queryCondition = 6;
                }

            }

        } else {

            queryCondition = 5;
        }
        return queryCondition;

    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        adaptor.swapCursor(data);
        int totalPofit = 0;

        if (data.moveToFirst()){
            do{
                int something = Integer.parseInt(data.getString(data.getColumnIndex(ResultsEntry.COLUMN_PROFIT)));
                totalPofit = totalPofit + something;

            }while(data.moveToNext());
        }

        TextView tv = (TextView) findViewById(R.id.sumOfProfits);
        String sProfit = " " + String.valueOf(totalPofit);
        tv.setText(sProfit);

    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        adaptor.swapCursor(null);

    }

    @Override
    public void getBuyIn(String buyIn) {
        this.myBuyIn = buyIn;

    }

    @Override
    public void getCashOut(String c) {
        this.myCashOut = c;

    }

    public void launchSearchActivity(View view) {
        Intent intent = new Intent(PlayerResultActivity.this, SearchActivity.class);
        startActivity(intent);

    }
}
