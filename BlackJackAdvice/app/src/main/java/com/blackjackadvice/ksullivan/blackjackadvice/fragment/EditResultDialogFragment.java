package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;

import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.activity.SearchActivity;
import com.blackjackadvice.ksullivan.blackjackadvice.databinding.ActivityDialogBinding;
import com.blackjackadvice.ksullivan.blackjackadvice.databinding.FragmentEditResultDialogBinding;
import com.blackjackadvice.ksullivan.blackjackadvice.model.Result;

/**
 * Created by ksullivan on 2/10/17.
 */

public class EditResultDialogFragment extends DialogFragment {

    Context myC;

    Result r;

    public interface EditResultDialogFragmentListener {
        void onDialogPositiveClick(DialogFragment fragment, String buyInListener, String cashOutListener);


        void onDialogNegativeClick(DialogFragment fragment);
    }

    public EditResultDialogFragmentListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (EditResultDialogFragment.EditResultDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement InsertResultDialogFragmentListener");
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
       r = (Result) getArguments().getSerializable(Constants.RESULT_OBJECT);
        myC = getActivity().getApplicationContext();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        //********!!!!!!NOTE THAT DATABINDING ISNT WORK QUITE RIGHT IN XML...SO SIMPLY GRABBING THE OBJECT'S FIELDS AND SETTING EM HERE
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.fragment_edit_result_dialog, null);
        final EditText mybuyIn = (EditText) alertView.findViewById(R.id.editBuyInResult);
        mybuyIn.setText(r.start_balance.get().toString());
        final EditText myCashOut = (EditText) alertView.findViewById(R.id.editCashOutResult);
        myCashOut.setText(r.end_balance.get().toString());
        FragmentEditResultDialogBinding binding =
                DataBindingUtil.inflate(LayoutInflater.from(myC), R.layout.fragment_edit_result_dialog, null, false);
        //todo i am getting the value here
        String sup = r.profit.get().toString();
        binding.setResult(r);

        builder.setView(alertView);


        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //EditText mybuyIn = (EditText) alertView.findViewById(R.id.editBuyInResult);

                String stringMyBuyIn = mybuyIn.getText().toString();
                String stringMyCashOut = myCashOut.getText().toString();
                listener.onDialogPositiveClick(EditResultDialogFragment.this, stringMyBuyIn, stringMyCashOut);

            }
        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogNegativeClick(EditResultDialogFragment.this);
                    }
                });




        Dialog alertDialog = builder.create();
        alertDialog.show();

        return alertDialog;
//
//        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        inflater = getActivity().getLayoutInflater();
//        builder.setView(inflater.inflate(R.layout.fragment_edit_result_dialog, null));
//
//
//        FragmentEditResultDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(myC), R.layout.fragment_edit_result_dialog, null, false);
//        r = (Result) getArguments().getSerializable(Constants.RESULT_OBJECT);
//        binding.setResult(r);
//
//        //v = inflater.inflate(R.layout.fragment_edit_result_dialog, null);
//
//
//
//        builder.setPositiveButton("Edit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                editData();
//                //how to go from dialogue fragment to an activity
//                //Toast.makeText(EditResultDialogFragment.this, "Entry deleted!", Toast.LENGTH_SHORT).show();
//                //Intent intent = new Intent(SinglePlayerResultActivity.class, MainActivity.class);
//                //startActivity(intent);
//
//            }
//        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                sa.launchSearchDateDialogFragment(v);
//
//            }
//        });
//
//        return builder.create();
    }

    private void editData() {


    }
}
