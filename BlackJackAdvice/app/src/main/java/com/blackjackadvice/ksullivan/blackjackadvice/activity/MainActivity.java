package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;

import com.blackjackadvice.ksullivan.blackjackadvice.CardListener;
import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.GameModeListener;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.DealerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PlayerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PlayerFragmentSecondCard;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.AdviceCalculatorService;

public class MainActivity extends AppCompatActivity implements CardListener, GameModeListener {

    public static final String DEALER_CARD = "dealerCard";
    public static final String PLAYER_CARD_A = "playerCardA";
    public static final String PLAYER_CARD_B = "playerCardB";
    private Button button;
    private FragmentManager manager;
    private String dealerCard;
    private String playerCard1;
    private String playerCard2;
    private AdviceCalculatorService adviceCalculatorService = new AdviceCalculatorService();

    private Switch blackjackVersion;
    boolean switchHitSoft17 = false;
    private String advice;
    private PlayerFragmentSecondCard mFragmentSecond;
    private DealerFragment mDealerFragment;
    private PlayerFragment mPlayerFragment;

    private Switch gameMode;
    private boolean practiceMode = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = (Button) findViewById(R.id.getAdviceBtn);
        blackjackVersion = (Switch) findViewById(R.id.blackjackVersion);
        gameMode = (Switch) findViewById(R.id.gameMode);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent!=null) {
            Bundle extras = getIntent().getExtras();
            if(extras!=null) {
                switchHitSoft17 = extras.getBoolean(Constants.GAME_VERSION);
                if (switchHitSoft17) {
                    blackjackVersion.setChecked(true);
                }
            }

        }


        //IF IN LANDSCAPE ORIENTATION A FAB IS ADDED AND THE ADVICE BUTTON REMOVED TO ACCOMMIDATE THE SMALLER SPACE
        if (getResources().getConfiguration().orientation == 2) {
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                  getHandAdvice(view);
                }
            });

        }

        manager = getFragmentManager();
        //addDealerFragment();

        if (savedInstanceState != null) {
            dealerCard =  savedInstanceState.getString(DEALER_CARD);
            playerCard1 = savedInstanceState.getString(PLAYER_CARD_A);
            playerCard2 = savedInstanceState.getString(PLAYER_CARD_B);
            mFragmentSecond = (PlayerFragmentSecondCard) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT);
            mPlayerFragment = (PlayerFragment) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT);
            mDealerFragment = (DealerFragment) manager.findFragmentByTag(Constants.DEALER_FRAGMENT_CARD);


        }

        //blackjackVersion.setBackgroundColor(isChecked? Color.GREEN : Color.WHITE);

        blackjackVersion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {


                switchHitSoft17 = isChecked;
            }
        });

            gameMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    practiceMode = isChecked;
                    startPracticeMode();
                }
            });



        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                getHandAdvice(v);

             }
        });

    }

    public void startPracticeMode() {

        Intent intent = new Intent(MainActivity.this, MainPracticeActivity.class);
        intent.putExtra(Constants.GAME_VERSION, switchHitSoft17);
        startActivity(intent);

    }


    public void getHandAdvice(View v) {

        if(dealerCard !=null && playerCard1 !=null && playerCard2!=null) {

            advice = adviceCalculatorService.getHandAdvice(dealerCard, playerCard1, playerCard2, switchHitSoft17);

            Snackbar.make(v, advice, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        } else  {
            Snackbar.make(v, Constants.SELECT_CARDS_IN_PLAY_MESSAGE, Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        mDealerFragment = (DealerFragment) manager.findFragmentByTag(Constants.DEALER_FRAGMENT_CARD);
        mPlayerFragment = (PlayerFragment) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT);
        mFragmentSecond = (PlayerFragmentSecondCard) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT);

        outState.putString(DEALER_CARD, dealerCard);
        outState.putString(PLAYER_CARD_A, playerCard1);
        outState.putString(PLAYER_CARD_B, playerCard2);

    }

    private void addPlayerFragmentSecondCard() {
        mFragmentSecond = new PlayerFragmentSecondCard();
        FragmentTransaction f = manager.beginTransaction();
        f.add(R.id.containerPlayerFragmentSecondCard, mFragmentSecond, Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT);
        f.commit();

    }

    private void addPlayerFragment() {
        mPlayerFragment = new PlayerFragment();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.add(R.id.containerPlayerFragment, mPlayerFragment, Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT);
        fragmentTransaction.commit();


    }

    private void addDealerFragment() {
        mDealerFragment = new DealerFragment();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.add(R.id.containerDealerFragment2, mDealerFragment, Constants.DEALER_FRAGMENT_CARD);
        fragmentTransaction.commit();

    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent i;

            if (id == R.id.action_go_to_player_result_activity) {
                i = new Intent(MainActivity.this, PlayerResultActivity.class );
                startActivity(i);
        } if (id == R.id.action_go_search) {
               i = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(i);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getDealerCard(String card) {

        this.dealerCard = card;
     //   sendDealerCardToAdviceFragment();


    }

    @Override
    public void getPlayerCardOne(String card) {
        this.playerCard1 = card;
        //sendPlayerCardOne();

    }



    @Override
    public void getPlayerCardTwo(String card) {
        this.playerCard2 = card;
        //sendPlayerCardTwo();

    }

    @Override
    public void getGameMode(Boolean b) {

    }
}
