//package com.blackjackadvice.ksullivan.blackjackadvice;
//
//import android.content.Context;
//import android.databinding.DataBindingUtil;
//import android.databinding.ObservableArrayList;
//import android.databinding.ObservableInt;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.Spinner;
//
//import com.blackjackadvice.ksullivan.blackjackadvice.databinding.ResultListItemBinding;
//import com.blackjackadvice.ksullivan.blackjackadvice.model.Result;
//
///**
// * Created by ksullivan on 2/8/17.
// */
//
//public class ResultListAdapter extends BaseAdapter {
//    public ObservableArrayList<Result> list;
//    private ObservableInt position = new ObservableInt();
//    private LayoutInflater inflater;
//    public ResultListAdapter(ObservableArrayList<Result> l) {
//
//        list = l;
//
//    }
//    public ResultListAdapter() {
//        list = new ObservableArrayList<Result>();
//    }
//    @Override
//    public int getCount() {
//
//        return list.size();
//    }
//    @Override
//    public Object getItem(int position) {
//
//        return list.get(position);
//    }
//    @Override
//    public long getItemId(int position) {
//        int id = list.get(position).id.get();
//        return id;
//    }
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//        if (inflater == null) {
//            inflater = (LayoutInflater) parent.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        }
//        ResultListItemBinding binding = DataBindingUtil.inflate(inflater, R.layout.list_item, parent, false);
//        binding.setResult(list.get(position));
//        return binding.getRoot();
//    }
//    //for the spinner
//    public int getPosition(Spinner spinner) {
//        return spinner.getSelectedItemPosition();
//    }
//
//    public int getPosition() {
//        //return 0;
//        return position.get();
//    }
//    public void setPosition(int position) {
//        this.position.set(position);
//    }
//}