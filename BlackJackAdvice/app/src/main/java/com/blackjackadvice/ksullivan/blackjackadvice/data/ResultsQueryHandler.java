package com.blackjackadvice.ksullivan.blackjackadvice.data;

import android.content.AsyncQueryHandler;
import android.content.ContentResolver;

/**
 * Created by ksullivan on 2/7/17.
 */

public class ResultsQueryHandler extends AsyncQueryHandler {
    public ResultsQueryHandler(ContentResolver cr) {
        super(cr);
    }
}
