package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.SearchDateDialogFragment;

public class SearchActivity extends AppCompatActivity implements SearchDateDialogFragment.SearchDateDialogFragmentListener {

    public static final String THIS_YEAR = "thisYear";
    public static final String THIS_MONTH = "thisMonth";
    public static final String THIS_DAY = "thisDay";
    public static final String PROFITS_ABOVE_$ = "Profits Above $";
    private SeekBar seekBar;
    private TextView textView;
    private SeekBar seekBarLoss;
    private TextView textViewLoss;
    private Button searchButton;
    int progressValueLoss;
    int progressValue;
    private Switch switchByOrder;
    boolean aOrder = false;
    private Button searchByDate;

    private int localMonth;
    private int localDay;
    private int localYear = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        searchButton = (Button) findViewById(R.id.buttonSearch);
        searchByDate = (Button) findViewById(R.id.buttonSearchByDate);
        switchByOrder = (Switch) findViewById(R.id.switchOrder);
        if (savedInstanceState != null) {
            localMonth = savedInstanceState.getInt(THIS_MONTH);
            localDay = savedInstanceState.getInt(THIS_DAY);
            localYear = savedInstanceState.getInt(THIS_YEAR);
        }


        setSupportActionBar(toolbar);
        showSeekBarMethodForWins();
        showSeekBarMethodForLoss();

        switchByOrder.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                //todo this line of code seems to be still running
                //blackjackVersion.setBackgroundColor(isChecked? Color.GREEN : Color.WHITE);
                aOrder = isChecked;
            }
        });

    }

    public void showSeekBarMethodForWins() {

        seekBar = (SeekBar) findViewById(R.id.seekBar5);
        seekBar.setMax(750);
        textView = (TextView) findViewById(R.id.textView3);
        Resources res = getResources();
        String text = String.format(res.getString(R.string.profits_above_$), seekBar.getProgress());
        textView.setText(text);

        //textView.setText(PROFITS_ABOVE_$ + seekBar.getProgress());


        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                progressValue = progress;
                Resources res = getResources();
                String text = String.format(res.getString(R.string.profits_above_$), progressValue);
                textView.setText(text);
                //textView.setText(PROFITS_ABOVE_$ + progressValue);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                Resources res = getResources();
                String text = String.format(res.getString(R.string.profits_above_$), progressValue);
                textView.setText(text);
                //textView.setText(PROFITS_ABOVE_$ + progressValue);


            }
        });

    }

    public void searchForResults(View view) {

        if ((progressValue > 0 && progressValueLoss > 0 )) {
            Toast.makeText(SearchActivity.this, "Choose only one of the selectors, or simply enter a date to search", Toast.LENGTH_SHORT).show();

        } else {
            Intent intent = new Intent(SearchActivity.this, PlayerResultActivity.class);

            if (progressValue > 0) {
                intent.putExtra(Constants.SEARCH_SORT_ORDER, aOrder);
                intent.putExtra(Constants.SEARCH_PROFITS, String.valueOf(progressValue));

            } else if (progressValueLoss > 0) {
                StringBuilder sb = new StringBuilder();
                sb.append("-" + String.valueOf(progressValueLoss));
                String lossMoney = sb.toString();
                intent.putExtra(Constants.SEARCH_SORT_ORDER, aOrder);
                intent.putExtra(Constants.SEARCH_PROFITS, lossMoney);

            }
            if(localYear > 0) {
                intent.putExtra(Constants.SEARCH_YEAR, localYear);
                intent.putExtra(Constants.SEARCH_DAY, localDay);
                intent.putExtra(Constants.SEARCH_MONTH, localMonth);
            }
            startActivity(intent);

        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(THIS_YEAR, localYear);
        outState.putInt(THIS_MONTH, localMonth);
        outState.putInt(THIS_DAY, localDay);

    }

    private void showSeekBarMethodForLoss() {
        seekBarLoss = (SeekBar) findViewById(R.id.seekBarLoss);
        seekBarLoss.setMax(750);
        textViewLoss = (TextView) findViewById(R.id.textViewLoss);
        Resources res = getResources();
        String text = String.format(res.getString(R.string.losses_above_$), seekBarLoss.getProgress());
        textViewLoss.setText(text);
        //textViewLoss.setText("Losses Above $" + seekBarLoss.getProgress());


        seekBarLoss.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onProgressChanged(SeekBar seekBarLoss, int progress, boolean fromUser) {
                progressValueLoss = progress;
                Resources res = getResources();
                String text = String.format(res.getString(R.string.losses_above_$), progressValueLoss);
                textViewLoss.setText(text);
                //textViewLoss.setText("Losses Above $" + progressValueLoss);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBarLoss) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBarLoss) {
                Resources res = getResources();
                String text = String.format(res.getString(R.string.losses_above_$), progressValueLoss);
                textViewLoss.setText(text);
                textViewLoss.setText("Losses Above $" + progressValueLoss);
                //textViewLoss.setText("Losses Above $" + progressValueLoss);

            }
        });

    }

    public void launchSearchDateDialogFragment(View view) {
        SearchDateDialogFragment s = new SearchDateDialogFragment();
        s.show(getFragmentManager(), "searchDateDialog");

    }

    @Override
    public void onDialogPositiveClick(DialogFragment dialog, int month, int year, int day) {
        localDay = day;
        localMonth = month;
        localYear = year;

    }

    @Override
    public void onDialogNegativeClick(DialogFragment fragment) {

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent i;

        if (id == R.id.action_go_to_main_activity) {
            i = new Intent(SearchActivity.this, MainActivity.class );
            startActivity(i);
        } if (id == R.id.action_go_to_player_result_activity) {
            i = new Intent(SearchActivity.this, PlayerResultActivity.class);
            startActivity(i);
        } if (id == R.id.action_go_search) {
            i = new Intent(SearchActivity.this, SearchActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }

}
