package com.blackjackadvice.ksullivan.blackjackadvice.adaptor;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CursorAdapter;
import android.widget.TextView;

import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract;

/**
 * Created by ksullivan on 2/7/17.
 */

public class ResultsCursorAdaptor extends CursorAdapter {
    public ResultsCursorAdaptor(Context context, Cursor c, boolean autoRequery) {
        super(context, c, autoRequery);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        //this is if there is nothing in the view
        return LayoutInflater.from(context).inflate(R.layout.list_item, parent, false);
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        TextView resultTextView = (TextView) view.findViewById(R.id.tvText);
        TextView resultTextViewProfit = (TextView) view.findViewById(R.id.tvTextProfit);
        int dateColumn = cursor.getColumnIndex(Contract.ResultsEntry.COLUMN_DATE);
        int profitColumn = cursor.getColumnIndex(Contract.ResultsEntry.COLUMN_PROFIT);
        String text = cursor.getString(dateColumn);
        String textProfit = cursor.getString(profitColumn);
        resultTextView.setText(text);
        resultTextViewProfit.setText(textProfit);

    }
}
