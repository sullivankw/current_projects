package com.blackjackadvice.ksullivan.blackjackadvice;

/**
 * Created by ksullivan on 3/1/17.
 */

public interface GameModeListener {

    void getGameMode(Boolean b);
}
