package com.blackjackadvice.ksullivan.blackjackadvice.versionLogic;

//import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic..AdviceCalculatorService;
import com.blackjackadvice.ksullivan.blackjackadvice.Constants;

/**
 * Created by ksullivan on 2/9/17.
 */

public class HitSoft17 {

    public String getHandAdvice(String dealerCard, String playerCardA, String playerCardB) {

        int dealerCardInt = convertDealerCard(dealerCard);

        int playerCardAInt = convertPlayerCard(playerCardA);

        int playerCardBInt = convertPlayerCard(playerCardB);

        int playerCardSum = playerCardAInt + playerCardBInt;


        //------------------HANDLES SOFT ACES---------------------------------
        if (playerCardA.equals("A") || playerCardB.equals("A") && playerCardA != playerCardB) {
            switch(dealerCardInt) {
                case 2:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 ||
                            playerCardSum == 16 || playerCardSum == 17) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 3:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 ||
                            playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    } else if (playerCardSum == 17) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 4:
                    if (playerCardSum == 13 || playerCardSum == 14) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    } else if (playerCardSum == 15 || playerCardSum == 16 || playerCardSum == 17) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 5:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 ) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 6:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 ) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 18 || playerCardSum == 19) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 7:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 ) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 8:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 ) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 9:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 || playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 10:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 || playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;
                case 11:
                    if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16 ||
                            playerCardSum == 17 || playerCardSum == 18) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 19 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 21) {
                        return Constants.PLAYER_GOT_BLACKJACK;
                    }
                    break;

            }
            //------------------HANDLES SPLITS ACES---------------------------------
        } else if (playerCardA.equals(playerCardB)) {
            switch (dealerCardInt) {
                case 2:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 12) {
                        return Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED;
                    } else if (playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 14 || playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                case 3:
                    if (playerCardSum == 4 || playerCardSum == 6) {
                        return Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED;
                    } else if (playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 14 || playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")
                            || playerCardSum == 12) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                case 4:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 12 || playerCardSum == 14
                            || playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 5:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 12 || playerCardSum == 14
                            || playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                case 6:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 12 || playerCardSum == 14
                            || playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                case 7:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 14 || playerCardSum == 16 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 8 || playerCardSum == 12) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 8:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 8 ||
                            playerCardSum == 12 || playerCardSum == 14) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 9:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 8 ||
                            playerCardSum == 12 || playerCardSum == 14) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 16 || playerCardSum == 18 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 10:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 8 || playerCardSum == 10 ||
                            playerCardSum == 12 || playerCardSum == 14) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 16 || playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    } else if (playerCardSum == 18 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                //Logic is for if a dealer shows an ace but does not reveal a blackjack
                case 11:
                    if (playerCardSum == 4 || playerCardSum == 6 || playerCardSum == 8 || playerCardSum == 10 ||
                            playerCardSum == 12 || playerCardSum == 14) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_SURRENDER_OR_SPLIT;
                    } else if (playerCardSum == 18 || playerCardSum == 20) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardA.equals("A")) {
                        return Constants.PLAYER_SHOULD_SPLIT;
                    }
                    break;
            }

        } else {
            //------------------HANDLES all REMAINING CASES---------------------------------
            switch (dealerCardInt) {
                case 2:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16
                            || playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    }
                    break;
                case 3:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8 ||
                            playerCardSum == 12) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16
                            || playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 9 || playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 4:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16
                            || playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 9 || playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 5:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16
                            || playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 9 || playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 6:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 || playerCardSum == 15 || playerCardSum == 16
                            || playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 9 || playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 7:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 ||
                            playerCardSum == 15 || playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 8:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 ||
                            playerCardSum == 15 || playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    }
                    break;
                case 9:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12 || playerCardSum == 13 || playerCardSum == 14 ||
                            playerCardSum == 15) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 10 || playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_SURRENDER_OR_HIT;
                    }
                    break;
                case 10:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12 || playerCardSum == 13 ||
                            playerCardSum == 14 || playerCardSum == 10) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 17 || playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 15 || playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_SURRENDER_OR_HIT;
                    }
                    break;
                case 11:
                    if (playerCardSum == 4 || playerCardSum == 5 || playerCardSum == 6 || playerCardSum == 7 || playerCardSum == 8
                            || playerCardSum == 9 || playerCardSum == 12 || playerCardSum == 13 ||
                            playerCardSum == 14 || playerCardSum == 10 ) {
                        return Constants.PLAYER_SHOULD_HIT;
                    } else if (playerCardSum == 18 || playerCardSum == 19 || playerCardSum == 20
                            || playerCardSum == 21) {
                        return Constants.PLAYER_SHOULD_STAY;
                    } else if (playerCardSum == 11) {
                        return Constants.PLAYER_SHOULD_DOUBLE_OR_HIT;
                    } else if (playerCardSum == 15 || playerCardSum == 16) {
                        return Constants.PLAYER_SHOULD_SURRENDER_OR_HIT;
                    } else if (playerCardSum == 17) {
                        return Constants.PLAYER_SHOULD_SURRENDER_OR_STAND;
                    }
                    break;
            }
        }
        return Constants.PLAYER_SHOULD_SPLIT;
    }

    private int convertDealerCard(String dealerCard) {
        //Note for Ace, I am simply converting it to 11. For a dealer the math doesn't matter
        //I simply need a way to flag the value of an ace and create the logic accordingly
        int dealerCardInt;

        if (dealerCard.equals("T") || dealerCard.equals("J") || dealerCard.equals("Q") || dealerCard.equals("K") ) {
            dealerCardInt = 10;
        } else if (dealerCard.equals("A")) {
            dealerCardInt = 11;
        } else {
            dealerCardInt = Integer.parseInt(dealerCard);
        }
        return dealerCardInt;
    }

    private int convertPlayerCard(String playerCard) {
        int playerCardInt;
        if (playerCard.equals("T") || playerCard.equals("J") || playerCard.equals("Q") || playerCard.equals("K")) {
            playerCardInt = 10;
        } else if (playerCard.equals("A")) {
            playerCardInt = 11;
        } else  {
            playerCardInt = Integer.parseInt(playerCard);
        }
        return  playerCardInt;
    }

}
