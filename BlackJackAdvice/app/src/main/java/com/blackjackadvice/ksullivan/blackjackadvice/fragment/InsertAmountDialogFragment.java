package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;
import android.app.AlertDialog;

import com.blackjackadvice.ksullivan.blackjackadvice.R;

/**
 * Created by ksullivan on 2/24/17.
 */

public class InsertAmountDialogFragment extends DialogFragment {

    InsertAmountDialogFragmentListener listener;

    public interface InsertAmountDialogFragmentListener {
        void onDialogPositiveClick(DialogFragment fragment, String buyInListener, String cashOutListener);
        //void onDialogPositiveClick(DialogFragment fragment, int month, int year, int day);

        void onDialogNegativeClick(DialogFragment fragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (InsertAmountDialogFragment.InsertAmountDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement InsertResultDialogFragmentListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.fragment_insert_amount_dialog, null);
        builder.setView(alertView);



        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                EditText mybuyIn = (EditText) alertView.findViewById(R.id.dialogStartingAmount);
                String stringMyBuyIn = mybuyIn.getText().toString();

                EditText myCashOut = (EditText) alertView.findViewById(R.id.dialogEndingAmount);
                String stringMyCashOut = myCashOut.getText().toString();
                listener.onDialogPositiveClick(InsertAmountDialogFragment.this, stringMyBuyIn, stringMyCashOut);
            }
        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogNegativeClick(InsertAmountDialogFragment.this);
                    }
                });

        Dialog alertDialog = builder.create();
        alertDialog.show();

        return alertDialog;

    }
}
