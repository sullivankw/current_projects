package com.blackjackadvice.ksullivan.blackjackadvice;

import android.widget.Button;

import java.util.Random;

/**
 * Created by ksullivan on 3/2/17.
 */

public class CardFragmentService {

    public void getRandomCard(Button buttonA, Button buttonB, Button buttonC, Button buttonD,
                                    Button buttonE, Button buttonF, Button buttonG, Button buttonH,
                                    Button buttonI, Button buttonJ, Button buttonK,
                                    Button buttonL, Button buttonM) {

        final Random rand = new Random();
        int cardNumber = rand.nextInt(13) + 1;
        switch (cardNumber) {
            case 1:
                buttonA.performClick();
                break;
            case 2:
                buttonB.performClick();
                break;
            case 3:
                buttonC.performClick();
                break;
            case 4:
                buttonD.performClick();
                break;
            case 5:
                buttonE.performClick();
                break;
            case 6:
                buttonF.performClick();
                break;
            case 7:
                buttonG.performClick();
                break;
            case 8:
                buttonH.performClick();
                break;
            case 9:
                buttonI.performClick();
                break;
            case 10:
                buttonJ.performClick();
                break;
            case 11:
                buttonK.performClick();
                break;
            case 12:
                buttonL.performClick();
                break;
            case 13:
                buttonM.performClick();
            default:
                break;
        }

    }
}
