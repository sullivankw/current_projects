package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;

import com.blackjackadvice.ksullivan.blackjackadvice.R;

/**
 * Created by ksullivan on 2/14/17.
 */

public class InsertResultDialogFragment extends DialogFragment {

    int year_x, month_x, day_x;



    public interface InsertResultDialogFragmentListener {
        void onDialogPositiveClick(DialogFragment fragment, int month, int year, int day);

        void onDialogNegativeClick(DialogFragment fragment);
    }

    InsertResultDialogFragmentListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (InsertResultDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement InsertResultDialogFragmentListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.fragment_insert_result_dialog, null);
        builder.setView(alertView);



        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatePicker d = (DatePicker) alertView.findViewById(R.id.datePickerInsert);
                int month = d.getMonth();
                int year = d.getYear();
                int day = d.getDayOfMonth();

                listener.onDialogPositiveClick(InsertResultDialogFragment.this, month
                , year, day);
            }
        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogNegativeClick(InsertResultDialogFragment.this);
                    }
                });

        Dialog alertDialog = builder.create();
        alertDialog.show();

        return alertDialog;
    }

    private DatePickerDialog.OnDateSetListener dpickerListner
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month + 1;
            day_x = dayOfMonth;

        }
    };

}

