package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RadioButton;

import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;

/**
 * Created by ksullivan on 2/24/17.
 */

public class GuessDialogFragment extends DialogFragment {

    GuessDialogFragmentListener listener;

    boolean setDefault = false;

    public interface GuessDialogFragmentListener {
        void onDialogPositiveClick(DialogFragment fragment, String string);

        void onDialogNegativeClick(DialogFragment fragment);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (GuessDialogFragment.GuessDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement InsertResultDialogFragmentListener");
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.fragment_guess_dialog, null);
        builder.setView(alertView);
        //THIS SETS A DEFAULT CHOICE AND ENSURES IF OK IT CLICKED A RESPONSE IS SENT
        RadioButton b = (RadioButton) alertView.findViewById(R.id.radioButtonhit);
        if (savedInstanceState == null) {
            b.setChecked(true);

        }

        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

                RadioButton b = (RadioButton) alertView.findViewById(R.id.radioButtonhit);
                RadioButton c = (RadioButton) alertView.findViewById(R.id.radioButtonBJ);
                //RadioButton d = (RadioButton) alertView.findViewById(R.id.radioButtonDoubleOrHIt);
                RadioButton e = (RadioButton) alertView.findViewById(R.id.radioButtonDoubleOrStand);
                RadioButton f = (RadioButton) alertView.findViewById(R.id.radioButtonSplit);
               // RadioButton g = (RadioButton) alertView.findViewById(R.id.radioButtonSplitIfDoubleJustHit);
                RadioButton h = (RadioButton) alertView.findViewById(R.id.radioButtonStay);
                //RadioButton i = (RadioButton) alertView.findViewById(R.id.radioButtonSurrenderOrHit);
                RadioButton j = (RadioButton) alertView.findViewById(R.id.radioButtonSurrenderOrSplit);
                //RadioButton k = (RadioButton) alertView.findViewById(R.id.radioButtonSurrenderOrStand);
                String userGuess = " ";

                //SETTING A DEFAULT CHOICE



                        if (b.isChecked()) {
                            userGuess = Constants.PLAYER_SHOULD_HIT;
                        } else if (c.isChecked()) {
                            userGuess = Constants.PLAYER_GOT_BLACKJACK;
                        } else if (e.isChecked()) {
                            userGuess = Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
                            //include 1. double/hit
                        } else if (f.isChecked()) {
                            //include 1.split if double/just hot
                            userGuess = Constants.PLAYER_SHOULD_SPLIT;
                        } else if (h.isChecked()) {
                            userGuess = Constants.PLAYER_SHOULD_STAY;
                        } else if (j.isChecked()) {
                            userGuess = Constants.PLAYER_SHOULD_SURRENDER_OR_SPLIT;
                            //1. surrender/hit, surrender stand
                        }




                listener.onDialogPositiveClick(GuessDialogFragment.this, userGuess);
            }
        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogNegativeClick(GuessDialogFragment.this);
                    }
                });

        Dialog alertDialog = builder.create();
        alertDialog.show();

        return alertDialog;

    }
}
