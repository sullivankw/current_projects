package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Fragment;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.blackjackadvice.ksullivan.blackjackadvice.CardListener;
import com.blackjackadvice.ksullivan.blackjackadvice.CardValues;
import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksullivan on 2/3/17.
 */

public class PlayerFragmentSecondCard extends Fragment implements CardValues {

    public static final String PLAYER_CARD_B = "playerCardB";
    private Button button1, button2, button3, button4, button5, button6, button7, button8, button9, button10, button11, button12, button13;

    private int buttonColorStandard = R.drawable.roundedshapebtn;

    private int buttonColorChanged = R.drawable.selectedroundedshapebtn;

    private String playerCard;

    private List<Button> buttonList = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_player_second_card, container, false);

        setCard(view);

        if (savedInstanceState != null) {
            playerCard = savedInstanceState.getString(PLAYER_CARD_B);
            //SO DOESNT NULL IN CASE THE USER HAS'NT PUT A SELECTION IN THIS PARTICULAR FRAGMENT BUT HAS IN THE OTHERS
            //THUS WE STILL GET A SAVEDINSTANCE
             if (playerCard != null) {

            for (int i = 0; i < buttonList.size(); i++) {
                if (playerCard.equalsIgnoreCase(buttonList.get(i).getText().toString())) {

                    buttonList.get(i).setBackgroundResource(buttonColorChanged);
                }

            }

         }

        }

        return view;
    }

    @Override
    public void setCard(View view) {

            //-------------------------------Player Button Actions-----------------------------

            button1 = (Button) view.findViewById(R.id.buttonTwoPlayer);
            buttonList.add(button1);
            button2 = (Button) view.findViewById(R.id.buttonThreePlayer);
            buttonList.add(button2);
            button3 = (Button) view.findViewById(R.id.buttonFourPlayer);
            buttonList.add(button3);
            button4 = (Button) view.findViewById(R.id.buttonFivePlayer);
            buttonList.add(button4);
            button5 = (Button) view.findViewById(R.id.buttonSixPlayer);
            buttonList.add(button5);
            button6 = (Button) view.findViewById(R.id.buttonSevenPLayer);
            buttonList.add(button6);
            button7 = (Button) view.findViewById(R.id.buttonEightPlayer);
            buttonList.add(button7);
            button8 = (Button) view.findViewById(R.id.buttonNinePlayer);
            buttonList.add(button8);
            button9 = (Button) view.findViewById(R.id.buttonTenPlayer);
            buttonList.add(button9);
            button10 = (Button) view.findViewById(R.id.buttonJackPlayer);
            buttonList.add(button10);
            button11 = (Button) view.findViewById(R.id.buttonQueenPlayer);
            buttonList.add(button11);
            button12 = (Button) view.findViewById(R.id.buttonKingPlayer);
            buttonList.add(button12);
            button13 = (Button) view.findViewById(R.id.buttonAcePlayer);
            buttonList.add(button13);


            button1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button1.setBackgroundResource(buttonColorChanged);
                    playerCard = button1.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button2.setBackgroundResource(buttonColorChanged);

                    playerCard = button2.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button3.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button3.setBackgroundResource(buttonColorChanged);

                    playerCard = button3.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button4.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button4.setBackgroundResource(buttonColorChanged);

                    playerCard = button4.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button5.setBackgroundResource(buttonColorChanged);

                    playerCard = button5.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button6.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button6.setBackgroundResource(buttonColorChanged);

                    playerCard = button6.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button7.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button7.setBackgroundResource(buttonColorChanged);

                    playerCard = button7.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button8.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button8.setBackgroundResource(buttonColorChanged);

                    playerCard = button8.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button9.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button9.setBackgroundResource(buttonColorChanged);

                    playerCard = button9.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button10.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button10.setBackgroundResource(buttonColorChanged);

                    playerCard = button10.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button11.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button11.setBackgroundResource(buttonColorChanged);

                    playerCard = button11.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button12.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button12.setBackgroundResource(buttonColorChanged);

                    playerCard = button12.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });

            button13.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setButtonBackgroundColor();
                    button13.setBackgroundResource(buttonColorChanged);

                    playerCard = button13.getText().toString();
                    CardListener listener = (CardListener) getActivity();
                    listener.getPlayerCardTwo(playerCard);
                }
            });


        }

    @Override
    public void setButtonBackgroundColor() {

        for (int i =0; i < buttonList.size(); i ++) {

            buttonList.get(i).setBackgroundResource(buttonColorStandard);

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PLAYER_CARD_B, playerCard);
    }
}
