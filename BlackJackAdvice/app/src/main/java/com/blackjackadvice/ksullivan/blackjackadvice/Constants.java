package com.blackjackadvice.ksullivan.blackjackadvice;

/**
 * Created by ksullivan on 2/2/17.
 */

public class Constants {

    public static final String CARD_MESSAGE_SUMMARY = " ";
    public static final String PLAYER_FRAGMENT_CARD_ONE_FRAGMENT = "playerFrag";
    public static final String PLAYER_FRAGMENT_CARD_TWO_FRAGMENT = "playerFragCardTwo";
    public static final String ADVICE_FRAGMENT = "adviceFrag";
    public static final String DEALER_FRAGMENT_CARD = "dealerFrag";
    public static final String SELECT_CARDS_IN_PLAY_MESSAGE = "Need both player cards and the exposed dealer\'s";

    //Messages in regards to displaying the actual move advice given in the AdviceCalculator

    public static final String PLAYER_SHOULD_HIT = "Hit it!";
    public static final String PLAYER_SHOULD_STAY = "Stay!";
    public static final String PLAYER_SHOULD_SPLIT = "You gotta split!";
    public static final String PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED = "You gotta split if double is allowed. If not, just hit!";
    public static final String PLAYER_SHOULD_SURRENDER_OR_SPLIT = "You gotta surrender if they will let ya...otherwise, split em!";
    public static final String PLAYER_SHOULD_SURRENDER_OR_HIT = "You gotta surrender if they will let ya...otherwise, hit!";
    public static final String PLAYER_SHOULD_SURRENDER_OR_STAND = "You gotta surrender if they will let ya...otherwise, stand!";
    public static final String PLAYER_SHOULD_DOUBLE_OR_HIT = "Double if ya can....otherwise just hit!";
    public static final String PLAYER_SHOULD_DOUBLE_OR_STAND = "Double if ya can....otherwise just stand!";
    public static final String PLAYER_GOT_BLACKJACK = "Blackjack!";
    public static final String PLACEHOLDER = "In general....just don't play!";



    //FOR SEARCH
    public static final String SEARCH_SORT_ORDER = "sortOrder";
    public static final String SEARCH_DAY = "searchDay";
    public static final String SEARCH_MONTH = "searchMonth";
    public static final String SEARCH_YEAR = "searchYear";
    public static final String SEARCH_PROFITS = "searchProfits";
    public static String RESULT_OBJECT = "resultObject";

    public static final String GAME_VERSION = "gameVersion";


    //FOR


}
