package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.DatePicker;
import android.widget.EditText;

import com.blackjackadvice.ksullivan.blackjackadvice.R;

/**
 * Created by ksullivan on 2/16/17.
 */

public class SearchDateDialogFragment extends DialogFragment {

    //Button dateBtn;
    int year_x, month_x, day_x;
    static final int DILOG_ID = 0;


    public interface SearchDateDialogFragmentListener {
        void onDialogPositiveClick(DialogFragment fragment, int month, int year, int day);

        void onDialogNegativeClick(DialogFragment fragment);
    }

    SearchDateDialogFragmentListener listener;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            // Instantiate the NoticeDialogListener so we can send events to the host
            listener = (SearchDateDialogFragmentListener) activity;
        } catch (ClassCastException e) {
            // The activity doesn't implement the interface, throw exception
            throw new ClassCastException(activity.toString() + " must implement InsertResultDialogFragmentListener");
        }
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View alertView = inflater.inflate(R.layout.fragment_search_date_dialog, null);
        builder.setView(alertView);



        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                DatePicker d = (DatePicker) alertView.findViewById(R.id.datePickerSearch);
                int month = d.getMonth();
                int year = d.getYear();
                int day = d.getDayOfMonth();

                listener.onDialogPositiveClick(SearchDateDialogFragment.this, month
                        , year, day);
            }
        })
                .setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        listener.onDialogNegativeClick(SearchDateDialogFragment.this);
                    }
                });

        Dialog alertDialog = builder.create();
        alertDialog.show();
        //alertDialog.getWindow().setLayout(800, 1400);
        //alertDialog.setTitle("Add an entry");

        return alertDialog;
    }

    private DatePickerDialog.OnDateSetListener dpickerListner
            = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            year_x = year;
            month_x = month + 1;
            day_x = dayOfMonth;
            //Toast.makeText(InsertResultDialogFragment.this, year_x + "/" + month_x + "/" + day_x,Toast.LENGTH_SHORT).show();
        }
    };




}
