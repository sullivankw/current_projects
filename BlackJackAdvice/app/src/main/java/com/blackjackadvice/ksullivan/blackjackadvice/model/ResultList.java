package com.blackjackadvice.ksullivan.blackjackadvice.model;

import android.databinding.ObservableArrayList;

import java.io.Serializable;

/**
 * Created by ksullivan on 2/8/17.
 */

public class ResultList implements Serializable {
    public final ObservableArrayList<Result> itemList;

    public ResultList() {

        itemList = new ObservableArrayList<>();
    }

    public ResultList(ObservableArrayList<Result> il) {

        itemList = il;
    }
}
