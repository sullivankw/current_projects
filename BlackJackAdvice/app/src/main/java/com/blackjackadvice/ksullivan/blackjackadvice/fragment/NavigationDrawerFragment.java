package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.blackjackadvice.ksullivan.blackjackadvice.R;

/**
 * Created by ksullivan on 2/10/17.
 */

public class NavigationDrawerFragment extends Fragment{
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_navigation_drawer, container, false);



        return view;
    }
}
