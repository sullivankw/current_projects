package com.blackjackadvice.ksullivan.blackjackadvice.data;

import android.content.ContentProvider;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract;

import static com.blackjackadvice.ksullivan.blackjackadvice.data.Contract.CONTENT_AUTHORITY;
import static com.blackjackadvice.ksullivan.blackjackadvice.data.Contract.PATH_RESULTS;

/**
 * Created by ksullivan on 2/6/17.
 */

public class ResultsProvider extends ContentProvider{

    //arbitrary values that are used to id what data we want
    private static final int RESULTS = 1;
    private static final int RESULTS_ID = 2;
    private static final int GUESS_STREAK = 3;

    //need a uri matcher, no match means that no known patterns have been sent to the matcher yet
    private static final UriMatcher uriMatcher =
            new UriMatcher(UriMatcher.NO_MATCH);
    //initializer that will run the first time anything is called in this class
    static {
        //this will retrieve the whole table
        uriMatcher.addURI(CONTENT_AUTHORITY, PATH_RESULTS, RESULTS);
        //get indy row in a table
        uriMatcher.addURI(CONTENT_AUTHORITY, PATH_RESULTS
                + "/#", RESULTS_ID);

    }
    private DatabaseHelper helper;

    @Override
    public boolean onCreate() {
        helper = new DatabaseHelper((getContext()));

        return false;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {
        SQLiteDatabase db = helper.getReadableDatabase();
        Cursor c;
        int match = uriMatcher.match(uri);
        switch(match) {
            case RESULTS:
                c = db.query(Contract.ResultsEntry.TABLE_NAME, projection,
                        selection, selectionArgs, null, null, sortOrder);
                break;
            case RESULTS_ID:
                selection = Contract.ResultsEntry._ID + "?";
                selectionArgs = new String[] {String.valueOf(ContentUris.parseId(uri))};
                c = db.query(Contract.ResultsEntry.TABLE_NAME, projection, selection,
                        selectionArgs, null, null, sortOrder);
                break;
            default:
                throw new IllegalArgumentException("Unknown Uri: " + uri);

        }
        //this allows our ui to know a change has been made...it listens
        c.setNotificationUri(getContext().getContentResolver(), uri);
        return c;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        return null;
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        int match = uriMatcher.match(uri);
        switch(match) {
            case RESULTS:
                return insertRecord(uri, values, Contract.ResultsEntry.TABLE_NAME);
            default:
                throw new IllegalArgumentException("Query unknown URI: " + uri);
        }

    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case RESULTS:
                return deleteRecord(uri, null, null, Contract.ResultsEntry.TABLE_NAME);
            case RESULTS_ID:
                return deleteRecord(uri, selection, selectionArgs, Contract.ResultsEntry.TABLE_NAME);
            default:
                throw new IllegalArgumentException("Insert unknown URI: " + uri);
        }
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        int match = uriMatcher.match(uri);
        switch (match) {
            case RESULTS:
                return updateRecord(uri, values, selection, selectionArgs, Contract.ResultsEntry.TABLE_NAME);
//            case RESULTS_ID:
//                return updateRecord(uri, selection, selectionArgs, Contract.ResultsEntry.TABLE_NAME);
            default:
                throw new IllegalArgumentException("Update unknown URI: " + uri);
        }
    }

    private Uri insertRecord(Uri uri, ContentValues values, String table) {
        //this time we need a writable database
        SQLiteDatabase db = helper.getWritableDatabase();
        long id = db.insert(table, null, values);
        if (id == -1) {

            return null;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        db.close();
        return ContentUris.withAppendedId(uri, id);
    }

    private int deleteRecord(Uri uri, String selection, String[] selectionArgs, String tableName) {
        //this time we need a writable database
        SQLiteDatabase db = helper.getWritableDatabase();
        int id = db.delete(tableName, selection, selectionArgs);
        if (id == -1) {

            return -1;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        db.close();
        return id;
    }

    private int updateRecord(Uri uri, ContentValues values, String selection, String[] selectionArgs, String tableName) {
        //this time we need a writable database
        SQLiteDatabase db = helper.getWritableDatabase();
        int id = db.update(tableName, values, selection, selectionArgs);
        if (id == 0) {

            return -1;
        }
        getContext().getContentResolver().notifyChange(uri, null);
        db.close();
        return id;
    }


}
