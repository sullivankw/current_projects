package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.DialogFragment;
import android.content.ContentValues;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract;
import com.blackjackadvice.ksullivan.blackjackadvice.data.ResultsQueryHandler;
import com.blackjackadvice.ksullivan.blackjackadvice.databinding.ActivityDialogBinding;


import com.blackjackadvice.ksullivan.blackjackadvice.fragment.SearchDateDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.model.Result;

public class DialogActivity extends AppCompatActivity implements SearchDateDialogFragment.SearchDateDialogFragmentListener  {
    public static final String THIS_RESULT = "thisResult";
    public static final String STRING_BUY_IN_TAG = "stringBuyInTag";
    public static final String STRING_CASH_OUT_TAG = "stringCashOutTag";
    private Result result;
    private EditText buyIn, cashOut;
    public String stringBuyIn;
    public String stringCashOut;
    private Button button;
    private SearchActivity s = new SearchActivity();

    //todo after onresume is claunched in this activity, the activity where back was hit is destroyed

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.AppTheme_DialogCustom);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dialog);
        buyIn = (EditText) findViewById(R.id.editStartingAmount);
        cashOut = (EditText) findViewById(R.id.editEndingAmount);
        button = (Button) findViewById(R.id.editDateBtn);

        Intent intent = getIntent();
        result = (Result) intent.getSerializableExtra(Constants.RESULT_OBJECT);

        ActivityDialogBinding binding = DataBindingUtil.setContentView(
                this, R.layout.activity_dialog);
        binding.setResult(result);

        if (savedInstanceState != null) {
            result = (Result) savedInstanceState.getSerializable(THIS_RESULT);
            buyIn.setText(savedInstanceState.getString(STRING_BUY_IN_TAG));
            cashOut.setText(savedInstanceState.getString(STRING_CASH_OUT_TAG));
        }

        buyIn = (EditText) findViewById(R.id.editStartingAmount);
        cashOut = (EditText) findViewById(R.id.editEndingAmount);
    }

    public void launchEditDatePicker(View view) {
        Toast.makeText(DialogActivity.this, "yep", Toast.LENGTH_LONG).show();
        SearchDateDialogFragment s = new SearchDateDialogFragment();
        s.show(getFragmentManager(), "searchDateDialog");

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        buyIn = (EditText) findViewById(R.id.editStartingAmount);
        cashOut = (EditText) findViewById(R.id.editEndingAmount);

        outState.putSerializable(THIS_RESULT, result);
        stringBuyIn = buyIn.getText().toString();
        outState.putString(STRING_BUY_IN_TAG, stringBuyIn);
        stringCashOut = cashOut.getText().toString();
        outState.putString(STRING_CASH_OUT_TAG, stringCashOut);
    }

    public void abortEdit(View view) {
        Toast.makeText(DialogActivity.this, "Edit Aborted!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(DialogActivity.this, MainActivity.class);
        startActivity(intent);
    }

    public void confirmEdit(View view) {

            buyIn = (EditText) findViewById(R.id.editStartingAmount);
            cashOut = (EditText) findViewById(R.id.editEndingAmount);

            String[] args = {String.valueOf(result.id.get())};
            ContentValues values = new ContentValues();
            values.put(Contract.ResultsEntry.COLUMN_START_BALANCE, buyIn.getText().toString());
            values.put(Contract.ResultsEntry.COLUMN_END_BALANCE, cashOut.getText().toString());

            values.put(Contract.ResultsEntry.COLUMN_PROFIT, Integer.parseInt(cashOut.getText().toString()) - Integer.parseInt(buyIn.getText().toString()));
            ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
            handler.startUpdate(1, null, Contract.ResultsEntry.CONTENT_URI, values, Contract.ResultsEntry._ID + " =?", args);
            Toast.makeText(DialogActivity.this, "Edit Saved", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(DialogActivity.this, MainActivity.class);
            startActivity(intent);

    }

    @Override
    public void onDialogPositiveClick(DialogFragment fragment, int month, int year, int day) {

    }

    @Override
    public void onDialogNegativeClick(DialogFragment fragment) {

    }
}
