package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract;
import com.blackjackadvice.ksullivan.blackjackadvice.data.ResultsQueryHandler;
import com.blackjackadvice.ksullivan.blackjackadvice.databinding.SinglePlayerResultActivityBinding;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.EditResultDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.model.Result;

/**
 * Created by ksullivan on 2/8/17.
 */

public class SinglePlayerResultActivity extends AppCompatActivity implements EditResultDialogFragment.EditResultDialogFragmentListener{
    public static final String INDIVIDUAL_RESULT = "individualResult";
    public static final String DELETE_CHECK_RESULT = "deleteCheckResult";
    public static final String EDIT_CHECK_RESULT = "editCheckResult";
    private Result result;
    private ResultsQueryHandler handler;
    private CheckBox deleteBox;
    private Boolean deleteBoxBoolean;
    private CheckBox editBox;
    private Boolean editBoxBoolean;
    private FragmentManager manager;
    private AlertDialog alert;
    private String buyIn, cashOut;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.single_player_result_activity);

        Intent intent = getIntent();
        result = (Result) intent.getSerializableExtra(Constants.RESULT_OBJECT);

        SinglePlayerResultActivityBinding binding = DataBindingUtil.setContentView(
                this, R.layout.single_player_result_activity);
        binding.setResult(result);
        deleteBox = (CheckBox) findViewById(R.id.deleteResult);
        editBox = (CheckBox) findViewById(R.id.editResult);
        initiateDeleteDialogBox();

        if (savedInstanceState != null) {
            result = (Result) savedInstanceState.getSerializable(INDIVIDUAL_RESULT);
            deleteBoxBoolean = savedInstanceState.getBoolean(DELETE_CHECK_RESULT);
            editBoxBoolean = savedInstanceState.getBoolean(EDIT_CHECK_RESULT);
            if (deleteBoxBoolean) {
                launchDeleteDialogBox();
            }
            if (editBoxBoolean) {

            }

        }

        editBox.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View v) {
                                           String resultId = String.valueOf(result.id.get());
                                           int id = Integer.parseInt(resultId);
                                           String date = String.valueOf(result.date.get());
                                           String startBalance = String.valueOf(result.start_balance.get());
                                           String endBalance = String.valueOf(result.end_balance.get());
                                           String profit = String.valueOf(result.profit.get());
                                           String[] args = {String.valueOf(result.id.get())};

                                           Result r = new Result(id, date, startBalance, endBalance, profit);

                                           Bundle b = new Bundle();
                                           b.putSerializable(Constants.RESULT_OBJECT, r);
                                           EditResultDialogFragment e = new EditResultDialogFragment();
                                           e.setArguments(b);
                                           e.show(getFragmentManager(), "editDialog");



            }
        });


    }

    public void launchDeleteDialogBox() {

        AlertDialog.Builder builder = new
                AlertDialog.Builder(SinglePlayerResultActivity.this);
        builder.setMessage(getString(R.string.delete_single_result_dialog)).setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        deleteData();
                        Toast.makeText(SinglePlayerResultActivity.this, "Entry deleted", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SinglePlayerResultActivity.this, PlayerResultActivity.class);
                        startActivity(intent);

                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        deleteBox.setChecked(false);

                    }
                });
        AlertDialog alert = builder.create();
        alert.setTitle(R.string.delete_single_result_title);
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.show();


    }

    public void initiateDeleteDialogBox() {
        deleteBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchDeleteDialogBox();

            }
        });


    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);

        //show it can be launched again without conflict on rotation
        if (alert != null) {
            alert.dismiss();
        }
        outState.putSerializable(INDIVIDUAL_RESULT, result);
        deleteBoxBoolean = deleteBox.isChecked();
        outState.putBoolean(DELETE_CHECK_RESULT, deleteBoxBoolean);
        editBoxBoolean = editBox.isChecked();
        outState.putBoolean(EDIT_CHECK_RESULT, editBoxBoolean);

    }

    private void editData() {

        String[] args = {String.valueOf(result.id.get())};
        ContentValues values = new ContentValues();
        values.put(Contract.ResultsEntry.COLUMN_END_BALANCE, 1);
        ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
        //pdateRecord(Uri uri, ContentValues values, String selection, String[] selectionArgs, String tableName)
        handler.startUpdate(1, null, Contract.ResultsEntry.CONTENT_URI, values, Contract.ResultsEntry._ID + " =?", args);
        //int numRows = db.update(Contract.ResultsEntry.TABLE_NAME, values, Contract.ResultsEntry._ID + " =?", args);

    }

    public void deleteData() {

        String[] args = {String.valueOf(result.id.get())};
        Uri uri = Uri.withAppendedPath(Contract.ResultsEntry.CONTENT_URI, String.valueOf((result.id.get())));
        ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
        String selection = Contract.ResultsEntry._ID + "=?";
        handler.startDelete(1, null, uri, selection, args);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_single_player_result_activity, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_go_home) {
            Intent intent = new Intent(SinglePlayerResultActivity.this, MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.action_go_to_player_result_activity) {
                Intent i = new Intent(SinglePlayerResultActivity.this, PlayerResultActivity.class);
                startActivity(i);
            }

            return super.onOptionsItemSelected(item);
        }

    public void getEditDialog(View v) {
        LayoutInflater inflater;
        AlertDialog.Builder builder = new AlertDialog.Builder(SinglePlayerResultActivity.this);
        inflater = this.getLayoutInflater();
        builder.setView(inflater.inflate(R.layout.fragment_edit_result_dialog, null))

                .setPositiveButton("Edit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        editData();

                        Toast.makeText(SinglePlayerResultActivity.this, "Edit saved!", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(SinglePlayerResultActivity.this, MainActivity.class);
                        startActivity(intent);

                    }
                }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                editBox.setChecked(false);

            }
        });
        AlertDialog alert = builder.create();
        alert.setTitle(R.string.delete_single_result_title);
        alert.setIcon(android.R.drawable.ic_dialog_alert);
        alert.show();

    }

    @Override
    public void onDialogPositiveClick(DialogFragment fragment, String buyInListener, String cashOutListener) {
        this.buyIn = buyInListener;
        this.cashOut = cashOutListener;
        confirmEdit();

    }

    @Override
    public void onDialogNegativeClick(DialogFragment fragment) {
        editBox.setChecked(false);


    }

    public void confirmEdit() {

        String[] args = {String.valueOf(result.id.get())};
        ContentValues values = new ContentValues();
        values.put(Contract.ResultsEntry.COLUMN_START_BALANCE, buyIn);
        values.put(Contract.ResultsEntry.COLUMN_END_BALANCE, cashOut);


        values.put(Contract.ResultsEntry.COLUMN_PROFIT, Integer.parseInt(cashOut) - Integer.parseInt(buyIn));
        ResultsQueryHandler handler = new ResultsQueryHandler(this.getContentResolver());
        //pdateRecord(Uri uri, ContentValues values, String selection, String[] selectionArgs, String tableName)
        handler.startUpdate(1, null, Contract.ResultsEntry.CONTENT_URI, values, Contract.ResultsEntry._ID + " =?", args);
        //int numRows = db.update(Contract.ResultsEntry.TABLE_NAME, values, Contract.ResultsEntry._ID + " =?", args);
        Toast.makeText(SinglePlayerResultActivity.this, "Edit Saved!", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(SinglePlayerResultActivity.this, PlayerResultActivity.class);
        startActivity(intent);



    }
}


