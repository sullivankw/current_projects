package com.blackjackadvice.ksullivan.blackjackadvice.model;

import android.databinding.ObservableField;
import android.databinding.ObservableInt;

import java.io.Serializable;
import java.util.Observable;

/**
 * Created by ksullivan on 2/8/17.
 */

public class Result implements Serializable {
    public ObservableInt id  = new ObservableInt();
    public ObservableField<String> date = new ObservableField<String>();
    public ObservableField<String> start_balance = new ObservableField<String>();
    public ObservableField<String> end_balance = new ObservableField<String>();
    public ObservableField<String> profit = new ObservableField<String>();

    public Result(int id, String date, String start_balance, String end_balance, String profit) {
        this.id.set(id);
        this.date.set(date);
        this.start_balance.set(start_balance);
        this.end_balance.set(end_balance);
        this.profit.set(profit);

    }

    public Result() {

    }

}
