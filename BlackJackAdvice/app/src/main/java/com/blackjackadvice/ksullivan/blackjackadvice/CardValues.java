package com.blackjackadvice.ksullivan.blackjackadvice;

import android.view.View;

/**
 * Created by ksullivan on 3/6/17.
 */

public interface CardValues {

    void setCard(View view);

    void setButtonBackgroundColor();
}
