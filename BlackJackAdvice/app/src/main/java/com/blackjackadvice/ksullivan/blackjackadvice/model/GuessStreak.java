package com.blackjackadvice.ksullivan.blackjackadvice.model;

import android.databinding.ObservableInt;

import java.io.Serializable;

/**
 * Created by ksullivan on 6/12/17.
 */

public class GuessStreak implements Serializable {

    //int needed?
    public ObservableInt id  = new ObservableInt();
    public ObservableInt highestConsecutiveGuesses  = new ObservableInt();
    public ObservableInt currentConsecutiveGuesses = new ObservableInt();

    public GuessStreak(int id, int highestConsecutiveGuesses, int currentConsecutiveGuesses ) {
        this.id.set(id);
        this.highestConsecutiveGuesses.set(highestConsecutiveGuesses);
        this.currentConsecutiveGuesses.set(currentConsecutiveGuesses);
    }
}
