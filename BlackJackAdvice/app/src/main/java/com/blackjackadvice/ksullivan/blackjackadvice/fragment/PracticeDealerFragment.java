package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blackjackadvice.ksullivan.blackjackadvice.CardFragmentService;
import com.blackjackadvice.ksullivan.blackjackadvice.CardListener;
import com.blackjackadvice.ksullivan.blackjackadvice.CardValues;
import com.blackjackadvice.ksullivan.blackjackadvice.GameModeListener;
import com.blackjackadvice.ksullivan.blackjackadvice.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by ksullivan on 2/2/17.
 */

public class PracticeDealerFragment extends Fragment implements GameModeListener, CardValues {

    public static final String DEALER_CARD = "dealerCard";

    private Button buttonA, buttonB, buttonC, buttonD, buttonE, buttonF, buttonG, buttonH, buttonI, buttonJ, buttonK, buttonL, buttonM;

    private String dealerCard;

    private int buttonColorStandard = R.drawable.roundedshapebtn;

    private int buttonPressedColor = R.drawable.selectedroundedshapebtn;

    private List<Button> buttonList = new ArrayList<>();

    Boolean gameModeSelected = false;

    private CardFragmentService service = new CardFragmentService();


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

       View view;

        view = inflater.inflate(R.layout.fragment_dealer, container, false);

        setCard(view);

        if (savedInstanceState != null) {
            dealerCard = savedInstanceState.getString(DEALER_CARD);

            if (dealerCard != null) {

                for (int i = 0; i < buttonList.size(); i++) {

                    if (dealerCard.equalsIgnoreCase(buttonList.get(i).getText().toString())) {

                        buttonList.get(i).setBackgroundResource(buttonPressedColor);
                    }

                }
            }
        }

        if (savedInstanceState == null) {

            service.getRandomCard(buttonA, buttonB, buttonC, buttonD, buttonE, buttonF, buttonG,
                    buttonH, buttonI, buttonJ, buttonK, buttonL, buttonM);

        }

        return view;
    }


    @Override
    public void setCard(View view) {

        //---------------------------Dealer Button Actions-----------------------------


        buttonA = (Button) view.findViewById(R.id.buttonTwo);
        buttonList.add(buttonA);
        buttonB = (Button) view.findViewById(R.id.buttonThree);
        buttonList.add(buttonB);
        buttonC = (Button) view.findViewById(R.id.buttonFour);
        buttonList.add(buttonC);
        buttonD = (Button) view.findViewById(R.id.buttonFive);
        buttonList.add(buttonD);
        buttonE = (Button) view.findViewById(R.id.buttonSix);
        buttonList.add(buttonE);
        buttonF = (Button) view.findViewById(R.id.buttonSeven);
        buttonList.add(buttonF);
        buttonG = (Button) view.findViewById(R.id.buttonEight);
        buttonList.add(buttonG);
        buttonH = (Button) view.findViewById(R.id.buttonNine);
        buttonList.add(buttonH);
        buttonI = (Button) view.findViewById(R.id.buttonTen);
        buttonList.add(buttonI);
        buttonJ = (Button) view.findViewById(R.id.buttonJack);
        buttonList.add(buttonJ);
        buttonK = (Button) view.findViewById(R.id.buttonQueen);
        buttonList.add(buttonK);
        buttonL = (Button) view.findViewById(R.id.buttonKing);
        buttonList.add(buttonL);
        buttonM = (Button) view.findViewById(R.id.buttonAce);
        buttonList.add(buttonM);


        buttonA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dealerCard = buttonA.getText().toString();
                setButtonBackgroundColor();
                buttonA.setBackgroundResource(buttonPressedColor);
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);


            }
        });

        buttonB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setButtonBackgroundColor();
                buttonB.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonB.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonC.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonC.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonD.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonD.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonE.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonE.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonF.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonF.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonG.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonG.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonH.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonH.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonI.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonI.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonJ.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonJ.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonK.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonK.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonL.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonL.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });

        buttonM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonM.setBackgroundResource(buttonPressedColor);
                dealerCard = buttonM.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getDealerCard(dealerCard);
            }
        });


    }
    @Override
    public void setButtonBackgroundColor() {

        for (int i =0; i < buttonList.size(); i ++) {

            buttonList.get(i).setBackgroundResource(buttonColorStandard);

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(DEALER_CARD, dealerCard);

    }

    @Override
    public void getGameMode(Boolean b) {
        this.gameModeSelected = b;
    }
}
