package com.blackjackadvice.ksullivan.blackjackadvice;

/**
 * Created by ksullivan on 2/3/17.
 */

public interface CardListener {

    void getDealerCard(String card);

    void getPlayerCardOne(String card);

    void getPlayerCardTwo(String card);
}
