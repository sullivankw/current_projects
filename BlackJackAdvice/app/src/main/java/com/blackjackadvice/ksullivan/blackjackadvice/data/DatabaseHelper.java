package com.blackjackadvice.ksullivan.blackjackadvice.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.blackjackadvice.ksullivan.blackjackadvice.data.Contract.ResultsEntry;

/**
 * Created by ksullivan on 2/6/17.
 */
public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "blackjackapp.db";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_RESULTS_CREATE=
            "CREATE TABLE " + ResultsEntry.TABLE_NAME + " (" +
                    ResultsEntry._ID + " INTEGER PRIMARY KEY, " +
                    ResultsEntry.COLUMN_DATE + " TEXT default CURRENT_TIMESTAMP, " + ResultsEntry.COLUMN_START_BALANCE
                    + " INTEGER, " + ResultsEntry.COLUMN_END_BALANCE + " INTEGER, " + ResultsEntry.COLUMN_PROFIT + " INTEGER " +
                    ")";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLE_RESULTS_CREATE);
        //onCreate(db);
        //i could add mock data here, that will be added the very first time the app is used only


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
