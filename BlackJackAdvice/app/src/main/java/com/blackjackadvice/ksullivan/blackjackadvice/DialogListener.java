package com.blackjackadvice.ksullivan.blackjackadvice;

/**
 * Created by ksullivan on 2/14/17.
 */

public interface DialogListener {

    void getBuyIn(String buyIn);

    void getCashOut(String cashOut);
}
