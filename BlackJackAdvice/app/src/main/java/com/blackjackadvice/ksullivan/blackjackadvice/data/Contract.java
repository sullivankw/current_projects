package com.blackjackadvice.ksullivan.blackjackadvice.data;

import android.net.Uri;
import android.provider.BaseColumns;

/**
 * Created by ksullivan on 2/6/17.
 */
public final class Contract {

    //package name + priver class is standard for authority name
    public static final String CONTENT_AUTHORITY = "com.blackjackadvice.ksullivan.blackjackadvice.resultsprovider";
    public static final String PATH_RESULTS = "results";
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);



    public static final class ResultsEntry implements BaseColumns {

        //below is the uri we call when we wanna mees with this table
        public static final Uri CONTENT_URI = Uri.withAppendedPath(BASE_CONTENT_URI, PATH_RESULTS);

        //below is the  acual content uri that is returning
        // content://com.blackjackadvice.ksullivan.blackjackadvice.resultsprovider/results

        //no, this uri
        //content://com.blackjackadvice.ksullivan.blackjackadvice.resultsprovider/results/14

        public static final String TABLE_NAME = "results";
        public static final String _ID = BaseColumns._ID;
        public static final String COLUMN_DATE = "date";
        public static final String COLUMN_START_BALANCE = "start_balance";
        public static final String COLUMN_END_BALANCE = "end_balance";
        public static final String COLUMN_PROFIT = "profit";


    }



}
