package com.blackjackadvice.ksullivan.blackjackadvice.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.blackjackadvice.ksullivan.blackjackadvice.CardFragmentService;
import com.blackjackadvice.ksullivan.blackjackadvice.CardListener;
import com.blackjackadvice.ksullivan.blackjackadvice.CardValues;
import com.blackjackadvice.ksullivan.blackjackadvice.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ksullivan on 2/2/17.
 */

public class PracticePlayerFragment extends Fragment implements CardValues {

    public static final String PLAYER_C = "playerC";
    private Button buttonA, buttonB, buttonC, buttonD, buttonE, buttonF, buttonG, buttonH, buttonI, buttonJ, buttonK, buttonL, buttonM;

    private String playerCard;

    private List<Button> buttonList = new ArrayList<>();

    private int buttonColorStandard = R.drawable.roundedshapebtn;

    private int buttonColorChanged = R.drawable.selectedroundedshapebtn;
    
    private CardFragmentService service = new CardFragmentService();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view;

        view = inflater.inflate(R.layout.fragment_player, container, false);

        setCard(view);

        if (savedInstanceState != null) {
            playerCard = savedInstanceState.getString(PLAYER_C);

            if (playerCard != null) {

                for (int i = 0; i < buttonList.size(); i++) {
                    if (playerCard.equalsIgnoreCase(buttonList.get(i).getText().toString())) {

                        buttonList.get(i).setBackgroundResource(buttonColorChanged);
                    }

                }
            }

        }
        //INSURES THAT RANDOM CARD ISNT NEW ON SCREEN ROTATION
        if (savedInstanceState == null) {

            service.getRandomCard(buttonA, buttonB, buttonC, buttonD, buttonE, buttonF, buttonG,
                    buttonH, buttonI, buttonJ, buttonK, buttonL, buttonM);
        }

        return view;
    }

    @Override
    public void setCard(View view) {
        //-------------------------------Player Button Actions-----------------------------

        buttonA = (Button) view.findViewById(R.id.buttonTwoPlayer);
        buttonList.add(buttonA);
        buttonB = (Button) view.findViewById(R.id.buttonThreePlayer);
        buttonList.add(buttonB);
        buttonC = (Button) view.findViewById(R.id.buttonFourPlayer);
        buttonList.add(buttonC);
        buttonD = (Button) view.findViewById(R.id.buttonFivePlayer);
        buttonList.add(buttonD);
        buttonE = (Button) view.findViewById(R.id.buttonSixPlayer);
        buttonList.add(buttonE);
        buttonF = (Button) view.findViewById(R.id.buttonSevenPLayer);
        buttonList.add(buttonF);
        buttonG = (Button) view.findViewById(R.id.buttonEightPlayer);
        buttonList.add(buttonG);
        buttonH = (Button) view.findViewById(R.id.buttonNinePlayer);
        buttonList.add(buttonH);
        buttonI = (Button) view.findViewById(R.id.buttonTenPlayer);
        buttonList.add(buttonI);
        buttonJ = (Button) view.findViewById(R.id.buttonJackPlayer);
        buttonList.add(buttonJ);
        buttonK = (Button) view.findViewById(R.id.buttonQueenPlayer);
        buttonList.add(buttonK);
        buttonL = (Button) view.findViewById(R.id.buttonKingPlayer);
        buttonList.add(buttonL);
        buttonM = (Button) view.findViewById(R.id.buttonAcePlayer);
        buttonList.add(buttonM);


        buttonA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonA.setBackgroundResource(buttonColorChanged);
                playerCard = buttonA.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonB.setBackgroundResource(buttonColorChanged);
                playerCard = buttonB.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setButtonBackgroundColor();
                buttonC.setBackgroundResource(buttonColorChanged);
                playerCard = buttonC.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonD.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                setButtonBackgroundColor();
                buttonD.setBackgroundResource(buttonColorChanged);
                playerCard = buttonD.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonE.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonE.setBackgroundResource(buttonColorChanged);
                playerCard = buttonE.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonF.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonF.setBackgroundResource(buttonColorChanged);
                playerCard = buttonF.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonG.setBackgroundResource(buttonColorChanged);
                playerCard = buttonG.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonH.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonH.setBackgroundResource(buttonColorChanged);
                playerCard = buttonH.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonI.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonI.setBackgroundResource(buttonColorChanged);
                playerCard = buttonI.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonJ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonJ.setBackgroundResource(buttonColorChanged);
                playerCard = buttonJ.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonK.setBackgroundResource(buttonColorChanged);
                playerCard = buttonK.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonL.setBackgroundResource(buttonColorChanged);
                playerCard = buttonL.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });

        buttonM.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                setButtonBackgroundColor();
                buttonM.setBackgroundResource(buttonColorChanged);
                playerCard = buttonM.getText().toString();
                CardListener listener = (CardListener) getActivity();
                listener.getPlayerCardOne(playerCard);
            }
        });


    }

    @Override
    public void setButtonBackgroundColor() {

        for (int i =0; i < buttonList.size(); i ++) {

            buttonList.get(i).setBackgroundResource(buttonColorStandard);

        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(PLAYER_C, playerCard);
    }
}
