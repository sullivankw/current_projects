package com.blackjackadvice.ksullivan.blackjackadvice.activity;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.CardListener;
import com.blackjackadvice.ksullivan.blackjackadvice.Constants;
import com.blackjackadvice.ksullivan.blackjackadvice.GameModeListener;
import com.blackjackadvice.ksullivan.blackjackadvice.R;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.DealerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.EditResultDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.GuessDialogFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PlayerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PlayerFragmentSecondCard;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PracticeDealerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PracticePlayerFragment;
import com.blackjackadvice.ksullivan.blackjackadvice.fragment.PracticePlayerFragmentSecondCard;
import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.AdviceCalculatorService;

import java.util.ArrayList;
import java.util.List;

public class MainPracticeActivity extends AppCompatActivity implements CardListener, GameModeListener, GuessDialogFragment.GuessDialogFragmentListener {

    private static final String DEALER_CARD = "dealerCard";
    private static final String PLAYER_CARD_A = "playerCardA";
    private static final String PLAYER_CARD_B = "playerCardB";
    private static final String PRACTICE_MODE = "practiceMode";
    private static final String VERSION = "version";
    private static final String COUNTER = "theCounter";
    private Button button;
    private FragmentManager manager;
    private String dealerCard;
    private String playerCard1;
    private String playerCard2;
    private AdviceCalculatorService adviceCalculatorService = new AdviceCalculatorService();

    private Switch blackjackVersion;
    boolean switchHitSoft17 = false;
    private String advice;
    private PracticePlayerFragmentSecondCard mFragmentSecond;
    private PracticeDealerFragment mDealerFragment;
    private PracticePlayerFragment mPlayerFragment;

    private Switch gameMode;
    private boolean practiceMode = false;

    private String guess;

    private View myView;

    private Boolean alwaysPracticeMode = true;

    private int counter;

    //should be an edit text
    private TextView counterText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.practice_activity_main);

        button = (Button) findViewById(R.id.getAdviceBtn);
        blackjackVersion = (Switch) findViewById(R.id.blackjackVersion);
        gameMode = (Switch) findViewById(R.id.gameMode);
        gameMode.setChecked(alwaysPracticeMode);
        //counterText = (TextView) findViewById(R.id.counterText);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        if (intent!=null) {
            Bundle extras = getIntent().getExtras();
            if(extras!=null) {
                counter = extras.getInt(COUNTER);
                switchHitSoft17 = extras.getBoolean(Constants.GAME_VERSION);
                if (switchHitSoft17) {
                    blackjackVersion.setChecked(true);
                }

            }



        }

        //IF IN LANDSCAPE ORIENTATION A FAB IS ADDED AND THE ADVICE BUTTON REMOVED TO ACCOMMIDATE THE SMALLER SPACE
        if (getResources().getConfiguration().orientation == 2) {
            FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    GuessDialogFragment g = new GuessDialogFragment();
                    sendView(view);
                    g.show(getFragmentManager(), "guessDialog");
                 // getHandAdvice(view);
                }
            });

        }

        manager = getFragmentManager();

        if (savedInstanceState != null) {
            dealerCard =  savedInstanceState.getString(DEALER_CARD);
            playerCard1 = savedInstanceState.getString(PLAYER_CARD_A);
            playerCard2 = savedInstanceState.getString(PLAYER_CARD_B);
            alwaysPracticeMode = savedInstanceState.getBoolean(PRACTICE_MODE);
            switchHitSoft17 = savedInstanceState.getBoolean(VERSION);
            counter = savedInstanceState.getInt(COUNTER);
//            mFragmentSecond = (PlayerFragmentSecondCard) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT_PRACTICE);
//            mPlayerFragment = (PlayerFragment) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT_PRACTICE);
//            mDealerFragment = (DealerFragment) manager.findFragmentByTag(Constants.DEALER_FRAGMENT_CARD_PRACTICE);

        }

        blackjackVersion.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                switchHitSoft17 = isChecked;
            }
        });

        gameMode.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                Intent intent = new Intent(MainPracticeActivity.this, MainActivity.class);
                intent.putExtra(Constants.GAME_VERSION, switchHitSoft17);
                startActivity(intent);
            }
        });

        button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                GuessDialogFragment g = new GuessDialogFragment();
                sendView(v);
                g.show(getFragmentManager(), "guessDialog");

             }
        });

    }

    public void getHandAdvice(View v) {

        boolean noMatch = true;

            if (dealerCard != null && playerCard1 != null && playerCard2 != null) {

                advice = adviceCalculatorService.getHandAdvice(dealerCard, playerCard1, playerCard2, switchHitSoft17);
                List<String> guessList = getRelatedGuesses(guess);

                for (int i = 0; i < guessList.size(); i ++) {

                    if (advice.equals(guessList.get(i))) {
                        noMatch = false;
                        counter = counter + 1;
                        //TODO potentially add a persistent table to store the last session high as well as the all time
                        Toast.makeText(MainPracticeActivity.this, "Correct!" , Toast.LENGTH_SHORT).show();
                        Toast consecutiveCorrect = Toast.makeText(MainPracticeActivity.this, counter + " in a row.", Toast.LENGTH_SHORT);
                        consecutiveCorrect.setGravity(Gravity.TOP, 0, 0);
                        consecutiveCorrect.show();
                        break;
                    }
                }
                     if (noMatch) {
                         counter = 0;
                         Toast.makeText(MainPracticeActivity.this, " Nope. It's " + advice, Toast.LENGTH_SHORT).show();
                         Toast consecutiveCorrect = Toast.makeText(MainPracticeActivity.this, counter + " in a row.", Toast.LENGTH_SHORT);
                         consecutiveCorrect.setGravity(Gravity.TOP, 0, 0);
                         consecutiveCorrect.show();
                     }

            } else {
                Snackbar.make(v, Constants.SELECT_CARDS_IN_PLAY_MESSAGE, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();

            }

        Intent intent = getIntent();
        intent.putExtra(COUNTER, counter);
        //TODO move the counter to display
        intent.putExtra(Constants.GAME_VERSION, switchHitSoft17);
        finish();
        startActivity(intent);

    }

    private List<String> getRelatedGuesses(String guess) {
        List<String> thisList = new ArrayList<>();
        thisList.add(guess);
        if (guess.equals(Constants.PLAYER_SHOULD_DOUBLE_OR_STAND)) {
            thisList.add(Constants.PLAYER_SHOULD_DOUBLE_OR_HIT);
        } else if (guess.equals(Constants.PLAYER_SHOULD_SPLIT)) {
            thisList.add(Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED);
        } else if (guess.equals(Constants.PLAYER_SHOULD_SURRENDER_OR_SPLIT)) {
            thisList.add(Constants.PLAYER_SHOULD_SURRENDER_OR_HIT);
            thisList.add(Constants.PLAYER_SHOULD_SURRENDER_OR_STAND);
        }

        return thisList;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.i("onSave", "onsaved called");

//        mDealerFragment = (DealerFragment) manager.findFragmentByTag(Constants.DEALER_FRAGMENT_CARD);
//        mPlayerFragment = (PlayerFragment) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT);
//        mFragmentSecond = (PlayerFragmentSecondCard) manager.findFragmentByTag(Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT);

        outState.putString(DEALER_CARD, dealerCard);
        outState.putString(PLAYER_CARD_A, playerCard1);
        outState.putString(PLAYER_CARD_B, playerCard2);
        outState.putBoolean(PRACTICE_MODE, alwaysPracticeMode);
        outState.putSerializable(VERSION, switchHitSoft17);
        outState.putInt(COUNTER, counter);

    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("onPause", "onpause called");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.i("onrestart", "onrestart called");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("onresume", "onresume called");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i("ondestory", "on destory called");
    }

    //    private void addPlayerFragmentSecondCard() {
//        mFragmentSecond = new PlayerFragmentSecondCard();
//        FragmentTransaction f = manager.beginTransaction();
//        f.add(R.id.containerPlayerFragmentSecondCard, mFragmentSecond, Constants.PLAYER_FRAGMENT_CARD_TWO_FRAGMENT);
//        f.commit();
//
//    }
//
//    private void addPlayerFragment() {
//        mPlayerFragment = new PlayerFragment();
//        FragmentTransaction fragmentTransaction = manager.beginTransaction();
//        fragmentTransaction.add(R.id.containerPlayerFragment, mPlayerFragment, Constants.PLAYER_FRAGMENT_CARD_ONE_FRAGMENT);
//        fragmentTransaction.commit();
//
//
//    }
//
//    private void addDealerFragment() {
//        mDealerFragment = new DealerFragment();
//        FragmentTransaction fragmentTransaction = manager.beginTransaction();
//        fragmentTransaction.add(R.id.containerDealerFragment2, mDealerFragment, Constants.DEALER_FRAGMENT_CARD);
//        fragmentTransaction.commit();
//
//    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        Intent i;

            if (id == R.id.action_go_to_player_result_activity) {
                i = new Intent(MainPracticeActivity.this, PlayerResultActivity.class );
                startActivity(i);
        } if (id == R.id.action_go_search) {
               i = new Intent(MainPracticeActivity.this, SearchActivity.class);
                startActivity(i);

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void getDealerCard(String card) {
        this.dealerCard = card;
     //   sendDealerCardToAdviceFragment();

    }

    @Override
    public void getPlayerCardOne(String card) {
        this.playerCard1 = card;
        //sendPlayerCardOne();

    }

    @Override
    public void getPlayerCardTwo(String card) {
        this.playerCard2 = card;
        //sendPlayerCardTwo();

    }

    @Override
    public void getGameMode(Boolean b) {

    }

    @Override
    public void onDialogPositiveClick(DialogFragment fragment, String userGuess) {
        this.guess = userGuess;
       getHandAdvice(myView);

    }

    @Override
    public void onDialogNegativeClick(DialogFragment fragment) {

    }

    private void sendView(View v) {
        this.myView = v;
    }

}
