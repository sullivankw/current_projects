package com.blackjackadvice.ksullivan.blackjackadvice;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.blackjackadvice.ksullivan.blackjackadvice.activity.PlayerResultActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.matcher.CursorMatchers.withRowString;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withTagValue;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;

/**
 * Created by ksullivan on 7/5/17.
 */

@RunWith(AndroidJUnit4.class)
public class CreateUserBlackjackEntryTest {
    //uses player result activity
    @Rule
    public ActivityTestRule<PlayerResultActivity> mActivityTestRule = new
            ActivityTestRule<PlayerResultActivity>(PlayerResultActivity.class);

    //how to click through the dialog frag
    @Test
    public void enterEntryTest() {
        //e2e test here
        onView(withId(R.id.addResultBtn))
                .perform(click());
        onView(withText("OK")).perform(click());
        onView(withText("Yes")).perform(click());
        onView(withId(R.id.dialogStartingAmount)).perform(typeText("300"), closeSoftKeyboard());
        onView(withId(R.id.dialogEndingAmount)).perform(typeText("500"), closeSoftKeyboard());
        onView(withId(R.id.dialogEndingAmount)).check(matches(withText("500")));
        onView(withText("OK")).perform(click());

        //todo figure how to use cursor matcher
       // onData(withRowString(1, "300")).inAdapterView(withId(R.id.showAllResults)).perform(click());


    }

}
