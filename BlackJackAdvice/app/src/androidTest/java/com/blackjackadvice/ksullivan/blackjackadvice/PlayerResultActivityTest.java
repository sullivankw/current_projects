package com.blackjackadvice.ksullivan.blackjackadvice;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.blackjackadvice.ksullivan.blackjackadvice.activity.PlayerResultActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

/**
 * Created by ksullivan on 7/3/17.
 */
@RunWith(AndroidJUnit4.class)
public class PlayerResultActivityTest {

    //this rule descibes how to load and end test after completiion

    @Rule
    public ActivityTestRule<PlayerResultActivity> mActivityTestRule = new
            ActivityTestRule<PlayerResultActivity>(PlayerResultActivity.class);

    //simply tests that button click opens the datepicker view
    @Test
    public void openDatePickerButtonTest() {
        onView(withId(R.id.addResultBtn))
                .perform(click());
        onView(withId(R.id.datePickerInsert)).check(matches(isDisplayed()));
    }
}
