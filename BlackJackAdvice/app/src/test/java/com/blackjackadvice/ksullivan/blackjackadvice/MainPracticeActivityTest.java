package com.blackjackadvice.ksullivan.blackjackadvice;

import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.widget.Toast;

import com.blackjackadvice.ksullivan.blackjackadvice.activity.MainPracticeActivity;
import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.AdviceCalculatorService;
import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.HitSoft17;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by ksullivan on 2/9/17.
 */

public class MainPracticeActivityTest {
    String dealerCard;
    String playerCardA;
    String playerCardB;
    HitSoft17 hitSoft17 = new HitSoft17();
    String advice;
    Boolean switchHitSoft17 = true;
    private String guess;

    AdviceCalculatorService a = new AdviceCalculatorService();

    @Before
    public void Setup(){
        dealerCard = "J";
        playerCardA = "4";
        playerCardB= "Q";
        guess = Constants.PLAYER_SHOULD_SPLIT;

    }
    @After
    public void TearDown(){

    }

    @Test
    public void testGetHandAdvice() throws Exception {

        advice = a.getHandAdvice(dealerCard, playerCardA, playerCardB, switchHitSoft17);
        assertNotEquals(guess, advice);

    }

    @Test
    public void testGetHandAdviceB() throws Exception {

        guess = Constants.PLAYER_SHOULD_DOUBLE_OR_STAND;
        advice = a.getHandAdvice(dealerCard, playerCardA, playerCardB, switchHitSoft17);
        assertNotEquals(guess, advice);

    }

    @Test
    public void testGetHandAdviceC() throws Exception {

        guess = Constants.PLAYER_SHOULD_SPLIT;
        dealerCard = "5";
        playerCardA = "8";
        playerCardB = "8";
        advice = a.getHandAdvice(dealerCard, playerCardA, playerCardB, switchHitSoft17);
        assertEquals(guess, advice);

    }

}
