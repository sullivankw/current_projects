package com.blackjackadvice.ksullivan.blackjackadvice;

import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.HitSoft17;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ksullivan on 2/9/17.
 */

public class HitSoft17Test {
    String dealerCard;
    String playerCardA;
    String playerCardB;
    HitSoft17 hitSoft17 = new HitSoft17();
    String advice;

    @Before
    public void Setup(){
        dealerCard = "J";
        playerCardA = "4";
        playerCardB= "Q";

    }
    @After
    public void TearDown(){

    }


    @Test
    public void testGetHandAdvice() throws Exception {

        advice = hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_HIT, advice);

    }

    @Test
    public void testGetHandAdviceB() throws Exception {
        dealerCard ="A";
        playerCardA = "A";
        playerCardB = "8";
        advice = hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_STAY, advice);


    }

    @Test
    public void testGetHandAdviceC() throws Exception {
        dealerCard ="6";
        playerCardA = "4";
        playerCardB = "4";
        advice = hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_SPLIT_IF_DOUBLE_ALLOWED, advice);


    }

    @Test
    public void testGetHandAdviceD() throws Exception {
        dealerCard ="K";
        playerCardA = "J";
        playerCardB = "5";
        advice = hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_SURRENDER_OR_HIT, advice);


    }

    @Test
    public void testGetHandAdviceE() throws Exception {
        dealerCard ="A";
        playerCardA = "8";
        playerCardB = "8";
        advice = hitSoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_SURRENDER_OR_SPLIT, advice);


    }

}
