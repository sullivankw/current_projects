package com.blackjackadvice.ksullivan.blackjackadvice;

import com.blackjackadvice.ksullivan.blackjackadvice.versionLogic.StaySoft17;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by ksullivan on 2/9/17.
 */

public class StaySoft17Test {
    String dealerCard;
    String playerCardA;
    String playerCardB;
    StaySoft17 staySoft17 = new StaySoft17();
    String advice;

    @Test
    public void testGetHandAdviceA() throws Exception {
        dealerCard ="A";
        playerCardA = "A";
        playerCardB = "6";
        advice = staySoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_HIT, advice);


    }

    @Test
    public void testGetHandAdviceB() throws Exception {
        dealerCard ="A";
        playerCardA = "T";
        playerCardB = "6";
        advice = staySoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_SURRENDER_OR_HIT, advice);


    }


    @Test
    public void testGetHandAdviceC() throws Exception {
        dealerCard ="A";
        playerCardA = "8";
        playerCardB = "8";
        advice = staySoft17.getHandAdvice(dealerCard, playerCardA, playerCardB);
        assertEquals(Constants.PLAYER_SHOULD_SPLIT, advice);


    }
}
