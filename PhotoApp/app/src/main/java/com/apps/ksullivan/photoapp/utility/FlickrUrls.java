package com.apps.ksullivan.photoapp.utility;


public class FlickrUrls {

    private static final String baseUrl = "https://www.flickr.com";

    private static final String apiKey = "6a8e0794c29c9c6641e639af0f2e0754";

    public static final String photoSearchUrl = baseUrl + "/services/rest/?format=json&nojsoncallback=1&api_key=" + apiKey
            + "&method=flickr.photos.search&tags=";

}
