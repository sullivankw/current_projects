package com.apps.ksullivan.photoapp.adaptor;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.apps.ksullivan.photoapp.GlideApp;

import com.apps.ksullivan.photoapp.R;
import com.apps.ksullivan.photoapp.model.Image;

import java.util.List;

public class ImageRecyclerViewAdaptor extends RecyclerView.Adapter<ImageRecyclerViewAdaptor.ImageItemViewHolder> {

    private List<Image> images;

    @Override
    public ImageItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(
                parent.getContext());

        return new ImageItemViewHolder(inflater.inflate(R.layout.image_item, parent, false));
    }

    public void setImageList(List<Image> images) {
        this.images = images;
        notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ImageItemViewHolder holder, int position) {

        GlideApp.with(holder.getImageView().getContext())
                .load(images.get(holder.getAdapterPosition()).getImageUrl())
                //.override(400, 50)
                .into(holder.getImageView());

    }

    @Override
    public int getItemCount() {
        if (images != null) {
            return  images.size();
        }
        return 0;
    }

    public class ImageItemViewHolder extends RecyclerView.ViewHolder {
        private ImageView imageView;

        public ImageItemViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.image);

        }
        public ImageView getImageView() {
            return imageView;
        }

    }

}
