package com.apps.ksullivan.photoapp;

import android.util.Log;

import com.apps.ksullivan.photoapp.model.FlickrSearchResponse;
import com.apps.ksullivan.photoapp.model.Image;
import com.apps.ksullivan.photoapp.utility.FlickrUrls;
import com.apps.ksullivan.photoapp.utility.LoggingInterceptor;
import com.apps.ksullivan.photoapp.viewmodel.ImageViewModel;
import com.squareup.moshi.JsonAdapter;
import com.squareup.moshi.Moshi;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;

public class ImageApiClient {
    private ImageViewModel viewModel;
    private static OkHttpClient client;
    private final Moshi moshi = new Moshi.Builder().build();
    private final JsonAdapter<FlickrSearchResponse> jsonAdapter = moshi.adapter(FlickrSearchResponse.class);
    private static ImageApiClient INSTANCE;

    private ImageApiClient(ImageViewModel viewModel) {
        this.viewModel = viewModel;
    }

    public static ImageApiClient getImageApiClient(ImageViewModel viewModel) {
        if (INSTANCE == null) {
            INSTANCE = new ImageApiClient(viewModel);
        }
        return INSTANCE;
    }

    public void getPhotosByTag(String tag) throws Exception {
        client = getOkHttpClient();
        Request request = new Request.Builder()
                .url(FlickrUrls.photoSearchUrl + tag)
                .build();

        client.newCall(request).enqueue(new Callback() {
            @Override public void onFailure(Call call, IOException e) {
                Log.d("flickrTimeOut", e.toString());
                viewModel.setServiceException(e);
            }

            @Override public void onResponse(Call call, Response response) throws IOException {
                    if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

                    ResponseBody responseBody = response.body();
                    if (responseBody != null && responseBody.source() != null) {
                        FlickrSearchResponse flickrSearchResponse = jsonAdapter.fromJson(responseBody.source());
                        if (flickrSearchResponse != null && flickrSearchResponse.getPhotos() != null) {
                            List<FlickrSearchResponse.Photo> photosResponse = flickrSearchResponse.getPhotos().getPhoto();

                            List<Image> images = new ArrayList<>();
                            for (FlickrSearchResponse.Photo photo : photosResponse) {
                                Image image = new Image();
                                image.setDescription(photo.getTitle().toUpperCase());
                                image.setUrl(String.valueOf(photo.getFarm()), photo.getServer(), photo.getId(), photo.getSecret());
                                images.add(image);
                            }
                            //if size is zero here, we made a sucessfull call but no results
                            viewModel.setImages(images);
                        }
                    }
                }
         });
      }

    private OkHttpClient getOkHttpClient() {
        if (client != null) {
            return client;
        }
        return new OkHttpClient.Builder()
                .connectTimeout(10, TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .readTimeout(10, TimeUnit.SECONDS)
                .addNetworkInterceptor(new LoggingInterceptor())
                .build();
    }

}
