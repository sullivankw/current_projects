package com.apps.ksullivan.photoapp.model;

public class Image {
    private String imageUrl;
    private String description;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setUrl(String farmId, String serverId, String id, String secret) {
        this.imageUrl = "https://farm" + farmId + ".staticflickr.com/"
                + serverId + "/" + id + "_" + secret
                + ".jpg";
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
