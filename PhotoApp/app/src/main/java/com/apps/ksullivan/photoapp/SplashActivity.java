package com.apps.ksullivan.photoapp;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

import java.util.Random;

/**
 * Only being used to pass some info to get some starting images into Main Activity upon app launch
 * It's a dumb progress bar. Wouldn't leave it like this, of course, if intended to be actual splash screen.
 */

public class SplashActivity extends AppCompatActivity {

    private static final int DELAY = 2000;
    public static final String INITIAL_SEARCH_TAG = "searchTag";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(new Runnable() {
            public void run() {

                Intent intent = new Intent(SplashActivity.this, MainActivity.class);
                intent.putExtra(INITIAL_SEARCH_TAG, createRandomImageSearchTag());
                startActivity(intent);
                finish();

            }
        }, DELAY);
    }

    public String createRandomImageSearchTag() {
        int random = new Random().nextInt(4);
        String[] tagArray = {"cat", "mountain", "city", "sky"};

        return tagArray[random];
    }

}
