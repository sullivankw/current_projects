package com.apps.ksullivan.photoapp;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputEditText;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.apps.ksullivan.photoapp.adaptor.ImageRecyclerViewAdaptor;
import com.apps.ksullivan.photoapp.model.Image;
import com.apps.ksullivan.photoapp.viewmodel.ImageViewModel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private Button searchButton;
    private Button filterButton;
    private ImageViewModel viewModel;
    private ImageRecyclerViewAdaptor recyclerViewAdaptor;
    private RecyclerView recyclerView;
    private TextInputEditText searchInput;
    private TextInputEditText filterInput;
    private CardView placeholder;
    private SwipeRefreshLayout swipeRefreshLayout;
    private RelativeLayout progressBarLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        findViews();
        configureRecyclerView();
        viewModel = ViewModelProviders.of(this).get(ImageViewModel.class);
        checkConnection();
        configureObservables();
        configureSearchButton();
        configureFilterButton();
        configureRefresh();
        Boolean connected = viewModel.isConnectedToNetwork().getValue();
        if (connected != null) {
            if (connected) {
                String initialSearchTag = getIntent().getStringExtra(SplashActivity.INITIAL_SEARCH_TAG);
                if (initialSearchTag != null) {
                    getPhotosForRecyclerView(initialSearchTag);
                }
            }
        }
        else {
            logConnectError();
        }
    }

    private void configureRefresh() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                checkConnection();
                Boolean connected = viewModel.isConnectedToNetwork().getValue();
                if (connected != null) {
                    if (connected) {
                        if (viewModel.getSearchInput() != null) {
                            getPhotosForRecyclerView(viewModel.getSearchInput());
                        } else {
                            //who wouldn't want to be welcomed back with dogs?
                            getPhotosForRecyclerView("dog");
                            }
                    } else
                        Toast.makeText(MainActivity.this, "Still no connection available", Toast.LENGTH_SHORT).show();
                } else {
                    logConnectError();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });
    }

    private void checkConnection() {
        ConnectivityManager manager = (ConnectivityManager) getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        if (manager == null) {
            viewModel.setIsConnectedToNetwork(false);
            Log.i("ConnectivityManager", "Connection Manager is null. Cannot establish connection");
        } else {
            NetworkInfo activeNetwork = manager.getActiveNetworkInfo();
            if (activeNetwork == null) {
                viewModel.setIsConnectedToNetwork(false);
                Log.i("NetworkInfo", "NetworkInfo is null. Cannot establish connection");
            } else {
                viewModel.setIsConnectedToNetwork(activeNetwork.isConnectedOrConnecting());
            }
        }
    }

    private void configureSearchButton() {
        searchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnection();
                Boolean connected = viewModel.isConnectedToNetwork().getValue();
                if (connected != null) {
                    try {
                        if (!connected) {
                            Toast.makeText(MainActivity.this, "No connection!", Toast.LENGTH_SHORT).show();
                        } else if (searchInput.getText().length() > 0) {
                            viewModel.setSearchInput(searchInput.getText().toString());
                            getPhotosForRecyclerView(searchInput.getText().toString());
                        } else {
                            Toast.makeText(MainActivity.this, "Enter search text!", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception e) {
                        Toast.makeText(MainActivity.this, "Can't retrieve search results at this time. Try again later.", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    logConnectError();
                }
            }
        });
    }

    private void configureRecyclerView() {
        recyclerView = findViewById(R.id.recyclerView);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, 1);
        recyclerView.addItemDecoration(dividerItemDecoration);
        recyclerViewAdaptor = new ImageRecyclerViewAdaptor();
        recyclerView.setLayoutManager(new GridLayoutManager(this, 2));
        recyclerView.setAdapter(recyclerViewAdaptor);
    }

    private void configureObservables() {

        viewModel.getImages().observe(this, new Observer<List<Image>>() {
            @Override
            public void onChanged(@Nullable List<Image> images) {
                if (images != null) {
                    progressBarLayout.setVisibility(View.GONE);
                    if (images.size() > 0) {
                        //if we search for term that returns no results we don't wanna clear current images from the rec view
                        recyclerViewAdaptor.setImageList(images);
                    } else {
                        Toast.makeText(MainActivity.this, "No Results Found", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        viewModel.isConnectedToNetwork().observe(this, new Observer<Boolean>() {
            @Override
            public void onChanged(@Nullable Boolean connected) {
                if (connected != null) {
                    //set before observing
                    if (connected) {
                        placeholder.setVisibility(View.GONE);
                    } else {
                        placeholder.setVisibility(View.VISIBLE);
                    }
                }

            }
        });

        viewModel.getServiceException().observe(this, new Observer<IOException>() {
            @Override
            public void onChanged(@Nullable IOException e) {
                if (e != null) {
                    //timeout is set to 10s, timeouts not uncommon
                    progressBarLayout.setVisibility(View.GONE);
                    Toast.makeText(MainActivity.this, "Unable to fetch photos from Flickr. Try again.", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void findViews() {
        searchInput = findViewById(R.id.search);
        searchButton = findViewById(R.id.button);
        filterButton = findViewById(R.id.filterButton);
        filterInput = findViewById(R.id.filterInput);
        placeholder = findViewById(R.id.placeholder);
        swipeRefreshLayout = findViewById(R.id.refreshLayout);
        progressBarLayout = findViewById(R.id.progressBarLayout);

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //no validation needed
            }
        });

        filterInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //no validation needed
            }
        });

    }

    private void configureFilterButton() {
        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkConnection();
                Boolean connected = viewModel.isConnectedToNetwork().getValue();
                if (connected != null) {
                    if (!connected) {
                        Toast.makeText(MainActivity.this, "No connection!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (filterInput.getText().length() == 0) {
                            Toast.makeText(MainActivity.this, "Enter term to filter", Toast.LENGTH_SHORT).show();
                        } else {
                            if (viewModel.getImages().getValue() != null && filterInput.getText().length() > 0) {
                                List<Image> filteredImages = new ArrayList<>();
                                for (Image i : viewModel.getImages().getValue()) {
                                    System.out.println(i.getDescription());
                                    if (i.getDescription().contains(filterInput.getText().toString().toUpperCase())) {
                                        filteredImages.add(i);
                                    }
                                }
                                if (filteredImages.size() > 0) {
                                    recyclerViewAdaptor.setImageList(filteredImages);
                                } else {
                                    Toast.makeText(MainActivity.this, "Can't filter the search tag further with " + filterInput.getText(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }
                logConnectError();
            }
        });
    }

    private void getPhotosForRecyclerView(String search) {
        try {
            progressBarLayout.setVisibility(View.VISIBLE);
            viewModel.getPhotoResultsByTag(search);
        } catch (Exception e) {
            //okhttp not catching here, maybe only for onResponse thrown errors
            e.printStackTrace();
        }
    }

    private void logConnectError() {
        Log.d("isConnectedToNetwork()", "isConnectedToNetwork() is null for some reason");
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        //not worth overriding really
        //could be used to store current search term if user navigated away from the whole process and was killed to free resources
        //can't count on the view model to store the data in that case
        //don't wanna store the entire search results list
    }
}
