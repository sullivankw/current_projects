package com.apps.ksullivan.photoapp.viewmodel;

import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.apps.ksullivan.photoapp.ImageApiClient;
import com.apps.ksullivan.photoapp.model.Image;

import java.io.IOException;
import java.util.List;

public class ImageViewModel extends ViewModel {
    private MutableLiveData<List<Image>> imagesList;
    private MutableLiveData<Boolean> isConnectedToNetwork;
    private ImageApiClient imageApiClient;
    private String searchInput;
    private MutableLiveData<IOException> serviceException;

    public MutableLiveData<IOException> getServiceException() {
        if (serviceException == null) {
            serviceException = new MutableLiveData<>();
            serviceException.setValue(null);
        }
        return serviceException;
    }

    public void setServiceException(IOException e) {
        if (serviceException == null) {
            serviceException = new MutableLiveData<>();
        }
        this.serviceException.postValue(e);
    }

    public MutableLiveData<List<Image>> getImages() {
        if (imagesList == null) {
            imagesList = new MutableLiveData<>();
            imagesList.setValue(null);
        }
        return imagesList;
    }

    public void setImages(List<Image> images) {
        if (imagesList == null) {
            imagesList = new MutableLiveData<>();

        }
        this.imagesList.postValue(images);
    }

    public MutableLiveData<Boolean> isConnectedToNetwork() {
        if (isConnectedToNetwork == null) {
            isConnectedToNetwork = new MutableLiveData<>();
            isConnectedToNetwork.setValue(false);
        }
        return isConnectedToNetwork;
    }

    public void setIsConnectedToNetwork(Boolean isConnected) {
        if (isConnectedToNetwork == null) {
            isConnectedToNetwork = new MutableLiveData<>();
        }
        this.isConnectedToNetwork.setValue(isConnected);
    }

    public void getPhotoResultsByTag(String tag) throws Exception {
        imageApiClient = ImageApiClient.getImageApiClient(this);
        imageApiClient.getPhotosByTag(tag);
    }

    public String getSearchInput() {
        return searchInput;
    }

    public void setSearchInput(String searchInput) {
        this.searchInput = searchInput;
    }
}
